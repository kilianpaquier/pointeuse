package com.model.entities;

/**
 * This class correspond to one manager
 */
public class Manager extends Employee {

    // Constructors

    /**
     * Constructor taking an id, a first name, a last name and an email
     * @param idEmployee the id of the employee
     * @param firstName the first name
     * @param lastName the last name
     * @param email the mail
     */
    public Manager(int idEmployee, String firstName, String lastName, String email){
        super(idEmployee, firstName, lastName, email);
    }

    /**
     * Constructor taking an id, a first name, a last name
     * @param idEmployee the id of the employee
     * @param firstName the first name
     * @param lastName the last name
     */
    public Manager(int idEmployee, String firstName, String lastName){
        super(idEmployee, firstName, lastName);
    }

    /**
     * Constructor taking a first name, a last name and an email
     * @param firstName the first name
     * @param lastName the last name
     * @param email the mail
     */
    public Manager(String firstName, String lastName, String email){
        super(firstName, lastName, email);
    }

    /**
     * Constructor taking a first name, a last name
     * @param firstName the first name
     * @param lastName the last name
     */
    public Manager(String firstName, String lastName){
        super(firstName, lastName, null);
    }

    /**
     * This function transform an employee into a manager
     * @param employee the employee to transform
     */
    public Manager(Employee employee){
        super(employee.getIdEmployee(), employee.getFirstName(), employee.getLastName(), employee.getEmail());
        setIdDepartment(employee.getIdDepartment());
        addCountHour(employee.getCountHour());
        setWorkDaysCheck(employee.getWorkDaysCheck());
        setCheckHours(employee.getCheckHours());
    }

}
