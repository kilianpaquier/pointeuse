package com.model.entities;

/**
 * This class correspond to the company's boss
 */
public class Boss extends People {
    // Constructors
    public Boss(String firstName, String lastName, String email) {
        super(firstName, lastName, email);
    }
}
