package com.view.centralApp.employees;

import javax.swing.*;

public class CreateEmployeeFrame extends JFrame {
    private JTextField firstName;
    private JTextField lastName;
    private JTextField mail;
    private JComboBox<Object> morningHour;
    private JComboBox<Object> morningMinute;
    private JComboBox<Object> afternoonMinute;
    private JComboBox<Object> afternoonHour;
    private JRadioButton managerRadioButton;
    private JRadioButton employeeRadioButton;
    private JButton createEmployeeButton;
    private JPanel globalPane;

    public CreateEmployeeFrame(String name) {
        super(name);
        setContentPane(globalPane);
    }

    public JTextField getFirstName() {
        return firstName;
    }

    public JTextField getLastName() {
        return lastName;
    }

    public JTextField getMail() {
        return mail;
    }

    public JComboBox<Object> getMorningHour() {
        return morningHour;
    }

    public JComboBox<Object> getMorningMinute() {
        return morningMinute;
    }

    public JComboBox<Object> getAfternoonMinute() {
        return afternoonMinute;
    }

    public JComboBox<Object> getAfternoonHour() {
        return afternoonHour;
    }

    public JRadioButton getManagerRadioButton() {
        return managerRadioButton;
    }

    public JRadioButton getEmployeeRadioButton() {
        return employeeRadioButton;
    }

    public JButton getCreateEmployeeButton() {
        return createEmployeeButton;
    }
}
