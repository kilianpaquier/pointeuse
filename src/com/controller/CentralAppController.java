package com.controller;

import com.model.entities.Company;
import com.model.entities.Department;
import com.model.entities.Employee;
import com.model.entities.Manager;
import com.model.tcp.TCPClient;
import com.model.tcp.TCPServerMainApp;
import com.model.time.TheoryCheckHours;
import com.model.time.WorkToday;
import com.view.centralApp.CentralApp;
import com.view.centralApp.checks.AddCheckFrame;
import com.view.centralApp.departments.DepartmentView;
import com.view.centralApp.employees.EmployeeView;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class CentralAppController {
    private static Company company;
    private int port = 8080;
    private static int incidence = 0;
    private String ip = "localhost";
    private static CentralApp centralApp;
    private TCPServerMainApp server;

    public int getPort() {
        return port;
    }

    public String getIp() {
        return ip;
    }

    public CentralApp getCentralApp() {
        return centralApp;
    }

    public Company getCompany() {
        return company;
    }

    public CentralAppController(String name) {
        centralApp = new CentralApp(name);
        initCompanyAndServer();
        initActions();
        updateAll();
        PrintWriter lock = null;
        try {
            lock = new PrintWriter(new File(".\\data\\mainAppServer.lock")); // Locking for the other main apps
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        lock.write("");
        lock.close();
    }


    /**
     * This function init all the actions of all JFrames, using the functions of the controller
     */
    private void initActions() {
        centralApp.addWindowListener(new WindowAdapter(){ public void windowClosing(WindowEvent e){saveCompanyOnClose(centralApp);}});
        // Action to change the IP and the Port
        {
            // Open the frame
            centralApp.getIpAndPort().addActionListener(e -> {
                openFrame(centralApp.getIpAndPortFrame());
                initSettings(centralApp.getIpAndPortFrame().getIpField(), centralApp.getIpAndPortFrame().getPortField());
                centralApp.setEnabled(false);
            });
            // Close the frame
            centralApp.getIpAndPortFrame().addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosed(WindowEvent e) {
                    centralApp.setEnabled(true);
                    centralApp.setVisible(true);
                }
            });
            // Action of the button
            centralApp.getIpAndPortFrame().getChangeSettingsButton().addActionListener(event -> {
                changeSettings(centralApp.getIpAndPortFrame().getIpField(), centralApp.getIpAndPortFrame().getPortField());
                centralApp.getIpAndPortFrame().dispose();
                centralApp.setEnabled(true);
                centralApp.setVisible(true);
            });
        }


        // Action to change the incidence
        {
            // Open the frame
            centralApp.getIncidence().addActionListener(e -> {
                openFrame(centralApp.getIncidenceFrame());
                initBoxIncidence(centralApp.getIncidenceFrame().getIncidenceBox());
                centralApp.setEnabled(false);
            });
            // Close the frame
            centralApp.getIncidenceFrame().addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosed(WindowEvent e) {
                    centralApp.setEnabled(true);
                    centralApp.setVisible(true);
                }
            });
            // Action of the button
            centralApp.getIncidenceFrame().getChangeIncidenceButton().addActionListener(event -> {
                changeIncidence(centralApp.getIncidenceFrame().getIncidenceBox());
                centralApp.getIncidenceFrame().dispose();
                centralApp.setEnabled(true);
                centralApp.setVisible(true);
                updateAll();
            });
        }


        // Action to add a missing check
        {
            centralApp.getAddCheck().addActionListener(e -> {
                AddCheckFrame addCheckFrame = new AddCheckFrame("Add Missing Check");
                addCheck(addCheckFrame, centralApp.getEmployeesTable(), centralApp.getChecksTable());
                centralApp.setEnabled(false);
                addCheckFrame.addWindowListener(new WindowAdapter() {
                    @Override
                    public void windowClosed(WindowEvent e) {
                        centralApp.setEnabled(true);
                        centralApp.setVisible(true);
                    }
                });
            });
        }


        // Action to import some checks
        {
            // Open the frame
            centralApp.getImportCheck().addActionListener(e -> {
                openFrame(centralApp.getImportCheckFrame());
                centralApp.setEnabled(false);
            });
            // Close the frame
            centralApp.getImportCheckFrame().addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosed(WindowEvent e) {
                    centralApp.setEnabled(true);
                    centralApp.setVisible(true);
                }
            });
            // Action of the button
            centralApp.getImportCheckFrame().getImportChecksButton().addActionListener(event -> {
                boolean succeed = importChecks(centralApp.getImportCheckFrame().getFilePath());
                if (succeed) {
                    centralApp.getImportCheckFrame().dispose();
                    centralApp.setVisible(true);
                    centralApp.setEnabled(true);
                    updateAll();
                }
            });
        }


        // Action to import Employees
        {
            // Open the frame
            centralApp.getImportEmployees().addActionListener(e -> {
                openFrame(centralApp.getImportEmployeesFrame());
                centralApp.setEnabled(false);
            });
            // Close the frame
            centralApp.getImportEmployeesFrame().addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosed(WindowEvent e) {
                    centralApp.setEnabled(true);
                    centralApp.setVisible(true);
                }
            });
            // Action of the button
            centralApp.getImportEmployeesFrame().getImportEmployeesButton().addActionListener(event -> {
                boolean succeed = importCSV(centralApp.getImportEmployeesFrame().getFilePath());
                if (succeed) {
                    centralApp.getImportEmployeesFrame().dispose();
                    centralApp.setVisible(true);
                    centralApp.setEnabled(true);
                    updateAll();
                }
            });
        }


        // Action to export employees
        {
            // Open the frame
            centralApp.getExportEmployees().addActionListener(e -> {
                openFrame(centralApp.getExportEmployeesFrame());
                centralApp.setEnabled(false);
            });
            // Close the frame
            centralApp.getExportEmployeesFrame().addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosed(WindowEvent e) {
                    centralApp.setEnabled(true);
                    centralApp.setVisible(true);
                }
            });
            // Action of the button
            centralApp.getExportEmployeesFrame().getExportEmployeesButton().addActionListener(event -> {
                boolean succeed = exportCSV(centralApp.getExportEmployeesFrame().getFilePath());
                if (succeed) {
                    centralApp.getExportEmployeesFrame().dispose();
                    centralApp.setVisible(true);
                    centralApp.setEnabled(true);
                    updateAll();
                }
            });
        }


        // Action to create an Employee
        {
            // Open the frame
            centralApp.getCreateEmployee().addActionListener(e -> {
                openFrame(centralApp.getCreateEmployeeFrame());
                initHoursBoxes(centralApp.getCreateEmployeeFrame().getMorningHour(), centralApp.getCreateEmployeeFrame().getMorningMinute());
                initHoursBoxes(centralApp.getCreateEmployeeFrame().getAfternoonHour(), centralApp.getCreateEmployeeFrame().getAfternoonMinute());
                initButtonEmployeeTime(centralApp.getCreateEmployeeFrame().getEmployeeRadioButton(), centralApp.getCreateEmployeeFrame().getManagerRadioButton());
                centralApp.setEnabled(false);
            });
            // Close the frame
            centralApp.getCreateEmployeeFrame().addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosed(WindowEvent e) {
                    centralApp.setVisible(true);
                    centralApp.setEnabled(true);
                    updateAll();
                }
            });
            // Action of the button
            centralApp.getCreateEmployeeFrame().getCreateEmployeeButton().addActionListener(event -> {
                createEmployee(centralApp.getCreateEmployeeFrame().getFirstName(), centralApp.getCreateEmployeeFrame().getLastName(), centralApp.getCreateEmployeeFrame().getMail(),
                        centralApp.getCreateEmployeeFrame().getMorningHour(), centralApp.getCreateEmployeeFrame().getAfternoonHour(), centralApp.getCreateEmployeeFrame().getMorningMinute(),
                        centralApp.getCreateEmployeeFrame().getAfternoonMinute(), centralApp.getCreateEmployeeFrame().getEmployeeRadioButton(),
                        centralApp.getCreateEmployeeFrame().getManagerRadioButton());
                updateAll();
            });
        }


        // Action to modify an Employee
        {
            // Open the frame
            centralApp.getModifyEmployee().addActionListener(e -> {
                openFrame(centralApp.getModifyEmployeeFrame());
                initEmployeeBox(centralApp.getModifyEmployeeFrame().getEmployeeBox());
                initEmployeeBoxDays(centralApp.getModifyEmployeeFrame().getDayBox());
                initDepartmentBox(centralApp.getModifyEmployeeFrame().getDepartmentBox());
                initHoursBoxes(centralApp.getModifyEmployeeFrame().getMorningHour(), centralApp.getModifyEmployeeFrame().getMorningMinute());
                initHoursBoxes(centralApp.getModifyEmployeeFrame().getAfternoonHour(), centralApp.getModifyEmployeeFrame().getAfternoonMinute());
                centralApp.setEnabled(false);
            });
            // Close the frame
            centralApp.getModifyEmployeeFrame().addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosed(WindowEvent e) {
                    centralApp.setVisible(true);
                    centralApp.setEnabled(true);
                    updateAll();
                }
            });
            // Action of the button
            centralApp.getModifyEmployeeFrame().getModifyEmployeeButton().addActionListener(e -> {
                modifyEmployee(centralApp.getModifyEmployeeFrame().getEmployeeBox(), centralApp.getModifyEmployeeFrame().getFirstName(), centralApp.getModifyEmployeeFrame().getLastName(), centralApp.getModifyEmployeeFrame().getMail(),
                        centralApp.getModifyEmployeeFrame().getDayBox(), centralApp.getModifyEmployeeFrame().getMorningHour(), centralApp.getModifyEmployeeFrame().getMorningMinute(),
                        centralApp.getModifyEmployeeFrame().getAfternoonHour(), centralApp.getModifyEmployeeFrame().getAfternoonMinute(),
                        centralApp.getModifyEmployeeFrame().getDepartmentBox());
                updateAll();
            });
        }


        // Action to delete an Employee
        {
            // Open the frame
            centralApp.getDeleteEmployee().addActionListener(e -> {
                openFrame(centralApp.getDeleteEmployeeFrame());
                initEmployeeBox(centralApp.getDeleteEmployeeFrame().getEmployeeBox());
                centralApp.setEnabled(false);
            });
            // Close the frame
            centralApp.getDeleteEmployeeFrame().addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosed(WindowEvent e) {
                    centralApp.setVisible(true);
                    centralApp.setEnabled(true);
                }
            });
            // Action of the button
            centralApp.getDeleteEmployeeFrame().getDeleteEmployeeButton().addActionListener(e -> {
                deleteEmployee(centralApp.getDeleteEmployeeFrame().getEmployeeBox());
                updateAll();
            });
        }


        // Action to create a Department
        {
            // Open the frame
            centralApp.getCreateDepartment().addActionListener(e -> {
                openFrame(centralApp.getCreateDepartmentFrame());
                initManagerBox(centralApp.getCreateDepartmentFrame().getManagerBox());
                centralApp.setEnabled(false);
            });
            // Action of the button
            centralApp.getCreateDepartmentFrame().getCreateDepartmentButton().addActionListener(e -> {
                createDepartment(centralApp.getCreateDepartmentFrame().getManagerBox(), centralApp.getCreateDepartmentFrame().getDepartmentName());
                updateAll();
            });
            // Close the frame
            centralApp.getCreateDepartmentFrame().addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosed(WindowEvent e) {
                    centralApp.setEnabled(true);
                    centralApp.setVisible(true);
                }
            });
        }


        // Action to modify a Department
        {
            // Open the frame
            centralApp.getModifyDepartment().addActionListener(e -> {
                openFrame(centralApp.getModifyDepartmentFrame());
                initDepartmentBox(centralApp.getModifyDepartmentFrame().getDepartmentBox());
                centralApp.setEnabled(false);
            });
            // Action of the button
            centralApp.getModifyDepartmentFrame().getModifyDepartmentButton().addActionListener(e -> {
                modifyDepartment(centralApp.getModifyDepartmentFrame().getDepartmentBox(), centralApp.getModifyDepartmentFrame().getDepartmentName());
                updateAll();
            });
            // Close the frame
            centralApp.getModifyDepartmentFrame().addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosed(WindowEvent e) {
                    centralApp.setVisible(true);
                    centralApp.setEnabled(true);
                }
            });
        }


        // Action to delete a Department
        {
            // Open the frame
            centralApp.getDeleteDepartment().addActionListener(e -> {
                openFrame(centralApp.getDeleteDepartmentFrame());
                initDepartmentBox(centralApp.getDeleteDepartmentFrame().getDepartmentBox());
                centralApp.setEnabled(false);
            });
            // Action of the button
            centralApp.getDeleteDepartmentFrame().getDeleteDepartmentButton().addActionListener(e -> {
                deleteDepartment(centralApp.getDeleteDepartmentFrame().getDepartmentBox());
                updateAll();
            });
            // Close the frame
            centralApp.getDeleteDepartmentFrame().addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosed(WindowEvent e) {
                    centralApp.setEnabled(true);
                    centralApp.setVisible(true);
                }
            });
        }


        // Action to filtrate the checks table
        {
            // Action of the button
            centralApp.getFiltrateButton().addActionListener(e -> filtrateCheckInOut(centralApp.getDepartmentBox(), centralApp.getDateBox(), centralApp.getChecksTable()));
        }

        // Action to update manually
        {
            centralApp.getUpdateButton().addActionListener(e -> updateAll());
        }

        // Action on tables
        {
            // Action on employees' table
            centralApp.getEmployeesTable().addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    EmployeeView employeeView = new EmployeeView("Employee's View");
                    boolean isOpen = mouseActionEmployees(employeeView, centralApp.getEmployeesTable());
                    if (isOpen)
                        centralApp.setEnabled(false);
                    // Close the frame
                    employeeView.addWindowListener(new WindowAdapter() {
                        @Override
                        public void windowClosed(WindowEvent e) {
                            centralApp.setEnabled(true);
                            centralApp.setVisible(true);
                        }
                    });
                }
            });

            // Action on department's table
            {
                // Action on the table
                centralApp.getDepartmentsTable().addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        DepartmentView departmentView = new DepartmentView("Department's View");
                        boolean isOpen = actionMouseDepartment(departmentView, centralApp.getDepartmentsTable());
                        if (isOpen)
                            centralApp.setEnabled(false);
                        // Close the frame
                        departmentView.addWindowListener(new WindowAdapter() {
                            @Override
                            public void windowClosed(WindowEvent e) {
                                centralApp.setVisible(true);
                                centralApp.setEnabled(true);
                            }
                        });
                        // Action on the button
                        departmentView.getSeeManagerButton().addActionListener(event -> {
                            if (!departmentView.getManagerLabel().getText().equals("Doesn't have manager")) {
                                EmployeeView employeeView = new EmployeeView("Manager's View");
                                seeEmployee(departmentView.getManagerLabel().getText().split(" ")[0], departmentView.getManagerLabel().getText().split(" ")[1], employeeView);
                                employeeView.setLocation(departmentView.getX() - 50, departmentView.getY() + 50);
                                departmentView.setEnabled(false);
                                // Close the manager's frame
                                employeeView.addWindowListener(new WindowAdapter() {
                                    @Override
                                    public void windowClosed(WindowEvent e) {
                                        centralApp.setVisible(true);
                                        departmentView.setEnabled(true);
                                        departmentView.setVisible(true);
                                    }
                                });
                            }
                        });
                        // Action on the employees' table
                        departmentView.getEmployeesTable().addMouseListener(new MouseAdapter() {
                            @Override
                            public void mouseClicked(MouseEvent e) {
                                EmployeeView employeeView = new EmployeeView("Employee's View");
                                boolean isOpen = mouseActionEmployees(employeeView, departmentView.getEmployeesTable());
                                employeeView.setLocation(departmentView.getX() - 50, departmentView.getY() + 50);
                                if (isOpen)
                                    departmentView.setEnabled(false);
                                // Close the frame
                                employeeView.addWindowListener(new WindowAdapter() {
                                    @Override
                                    public void windowClosed(WindowEvent e) {
                                        centralApp.setVisible(true);
                                        departmentView.setEnabled(true);
                                        departmentView.setVisible(true);
                                    }
                                });
                            }
                        });
                    }
                });
                // Another action on the table
                centralApp.getDepartmentsTable().addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        EmployeeView employeeView = new EmployeeView("Manager's View");
                        boolean isOpen = actionMouseDepartment(employeeView, centralApp.getDepartmentsTable());
                        if (isOpen)
                            centralApp.setEnabled(false);
                        // Close the frame
                        employeeView.addWindowListener(new WindowAdapter() {
                            @Override
                            public void windowClosed(WindowEvent e) {
                                centralApp.setEnabled(true);
                                centralApp.setVisible(true);
                            }
                        });
                    }
                });
            }

            // Action on checks' table
            {
                centralApp.getChecksTable().addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        EmployeeView employeeView = new EmployeeView("Employee's View");
                        boolean isOpen = mouseActionEmployees(employeeView, centralApp.getChecksTable());
                        if (isOpen)
                            centralApp.setEnabled(false);
                        // Close the frame
                        employeeView.addWindowListener(new WindowAdapter() {
                            @Override
                            public void windowClosed(WindowEvent e) {
                                centralApp.setEnabled(true);
                                centralApp.setVisible(true);
                            }
                        });
                    }
                });
            }
        }
    }


    /**
     * This function, static because used in the server, update all the tables
     */
    private static void updateAll() {
        initDateBox(centralApp.getDateBox());
        initDepartmentBox(centralApp.getDepartmentBox());
        initTableChecks(centralApp.getChecksTable());
        initEmployeesTable(centralApp.getEmployeesTable());
        initTableDepartment(centralApp.getDepartmentsTable());
        initDepartmentTableWithoutManager(centralApp.getDepartmentEvent());
        initTableEmployeesWithoutDepartment(centralApp.getEmployeesEvent());
    }


    /**
     * This function init the company and open the server
     */
    public void initCompanyAndServer() {
        // Creating the company
        company = new Company();
        try {
            // Reading the datas
            company.readDatas();
            readConf();
        } catch (IOException e) {
            e.printStackTrace();
        }

        server = new TCPServerMainApp(ip, port);
        syncEmployees();
        new Thread(server).start();
    }


    /**
     * This function save the company and the configuration into multiple CSV files
     * @param frame the main frame
     */
    public void saveCompanyOnClose(JFrame frame){
        // Verifying if the user really want to close the main app
        int reponse = JOptionPane.showConfirmDialog(frame,
                "Do you want to stop the application ?",
                "Confirmation",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE);
        if (reponse==JOptionPane.YES_OPTION){
            // Closing the main app
            frame.dispose();
            try {
                // Saving the datas and closing the server
                company.saveDatas();
                saveConf(); // Saving the configuration of the server
                server.close();
                new File(".\\data\\mainAppServer.lock").delete();

                // Change settings in the tracker server
                TCPClient client = new TCPClient("localhost", 8090);
                client.setInformation(ip + " " + this.port);
                client.run();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * This function synchronize the employees with the server
     */
    private void syncEmployees() {
        server.setEmployees(company.getEmployees());
    }


    /**
     * This function read the configuration from a file
     */
    private void readConf() {
        Properties properties = new Properties(); // Creating the properties
        try {
            properties.loadFromXML(new FileInputStream(".\\data\\conf.xml")); // Reading from the file XML
            // Reading the information with the keys
            ip = properties.getProperty("IP");
            port = Integer.valueOf(properties.getProperty("Port"));
            incidence = Integer.valueOf(properties.getProperty("Incidence"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * This function save the configuration into a file
     */
    private void saveConf() {
        Properties properties = new Properties(); // Creating the properties
        // Setting the keys and information for each propertie
        properties.setProperty("IP", ip);
        properties.setProperty("Port", String.valueOf(port));
        properties.setProperty("Incidence", String.valueOf(incidence));
        try {
            properties.storeToXML(new FileOutputStream(".\\data\\conf.xml"), null); // Storing the information inside a XML file
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * This function open a frame
     * @param frame the frame to open
     */
    public void openFrame(JFrame frame) {
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.pack();
        frame.setSize(400,400);
        frame.setLocationRelativeTo(frame.getOwner());
        frame.setResizable(false);
        frame.setVisible(true);
    }


    /**
     * This function init the box with all the managers who are not managing
     * @param managerBox the box where it will be all the first and last name of the managers
     */
    public void initManagerBox(JComboBox<Object> managerBox) {
        managerBox.removeAllItems();
        managerBox.addItem("");

        // Getting the list of the managers who are not managing and the employees
        List<Employee> listManagers = company.getManagersNotManagingWithEmployees();
        for(Employee employee : listManagers)
            managerBox.addItem(employee.getFirstName()+" "+employee.getLastName());
    }


    /**
     * This function init a box with all the days of the week
     * @param dayBox the box where it will be the days
     */
    public void initEmployeeBoxDays(JComboBox<Object> dayBox) {
        dayBox.removeAllItems();
        dayBox.addItem("");
        for(DayOfWeek dayOfWeek : DayOfWeek.values()){
            if (dayOfWeek == DayOfWeek.SATURDAY || dayOfWeek == DayOfWeek.SUNDAY)
                continue;
            dayBox.addItem(dayOfWeek);
        }
    }


    /**
     * This function init the box with all the departments' names
     * @param departmentBox the box where it will be all the names
     */
    public static void initDepartmentBox(JComboBox<Object> departmentBox) {
        departmentBox.removeAllItems();
        departmentBox.addItem("");
        for (Department department : company.getDepartments()) {
            departmentBox.addItem(department.getDepartmentName());
        }
    }


    /**
     * This function init the box with all the first and last name of the employees
     * @param employeeBox the box
     */
    public void initEmployeeBox(JComboBox<Object> employeeBox) {
        employeeBox.removeAllItems();
        employeeBox.addItem("");
        for (Employee employee : company.getEmployees()){
            employeeBox.addItem(employee.getFirstName()+" "+employee.getLastName());
        }
    }


    /**
     * This function create the group of the two buttons in the frame of an employee's creation
     * @param employeeButton the button to create an employee
     * @param managerButton the button to create a manager
     */
    public void initButtonEmployeeTime(JRadioButton employeeButton, JRadioButton managerButton) {
        // Creating the group of RadioButton inside the employee creator's panel
        ButtonGroup groupEmployeeManager = new ButtonGroup();
        groupEmployeeManager.add(employeeButton);
        groupEmployeeManager.add(managerButton);
        groupEmployeeManager.clearSelection();
    }


    /**
     * This function init the boxes with the hour and the minutes, especially to create or modify an employee
     * @param hourBox the box with the hour
     * @param minuteBox the box with the minutes
     */
    public void initHoursBoxes(JComboBox<Object> hourBox, JComboBox<Object> minuteBox) {
        // Creating the hours
        String[] hoursMorning = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"};
        String[] minutes = {"0", "15", "30", "45"};

        // Removing all items and adding the first
        hourBox.removeAllItems();
        minuteBox.removeAllItems();
        hourBox.addItem("");
        minuteBox.addItem("");

        // Adding each hour and minute to their boxes
        for (String aHoursAfternoon : hoursMorning)
            hourBox.addItem(aHoursAfternoon);
        for (String aMinutes : minutes)
            minuteBox.addItem(aMinutes);
    }


    /**
     * This function init the box with the current incidence and the possible incidence
     * @param incidenceBox the box where it will be the options
     */
    public void initBoxIncidence(JComboBox<Object> incidenceBox) {
        String[] minutes = {"15", "30", "45"};
        incidenceBox.removeAllItems();
        incidenceBox.addItem(incidence);
        for (String minute : minutes) {
            incidenceBox.addItem(minute);
        }
    }


    /**
     * This function init the box with the last 30 days
     * @param dateBox the box where it will be all the last 30 days
     */
    public static void initDateBox(JComboBox<Object> dateBox) {
        dateBox.removeAllItems();
        dateBox.addItem("");
        dateBox.addItem("All Checks");
        LocalDate today = LocalDate.now();

        // Setting inside the box the last 30 days
        for (int index = today.getDayOfYear(); index > today.getDayOfYear() - 30; index--) {
            dateBox.addItem(LocalDate.ofYearDay(today.getYear(), index));
        }
    }


    /**
     * This function init the JTextField with the currents IP and port of the server
     * @param ipField the field with the ip
     * @param portField the field with the port
     */
    public void initSettings(JTextField ipField, JTextField portField) {
        ipField.setText(ip);
        portField.setText(String.valueOf(port));
    }


    /**
     * This function init the box of all the days of checks the employee did
     * @param dateBox the box with all the days of checks
     * @param employee the employee who had checked
     */
    public void initDateBoxPerEmployee(JComboBox<Object> dateBox, Object employee) {
        if(!String.valueOf(employee).equals("")) {
            Employee employee1 = company.searchEmployee(String.valueOf(employee).split(" ")[0], String.valueOf(employee).split(" ")[1]);
            dateBox.removeAllItems();
            dateBox.addItem("");
            for (WorkToday today : employee1.getWorkDaysCheck()) {
                dateBox.addItem(today.getDate().toString());
            }
        }
        else {
            dateBox.removeAllItems();
        }
    }


    /**
     * This function init a simple box with the morning and the afternoon, it's used when the user want to add a missing check
     * @param timeBox the box with the morning and the afternoon
     */
    public void initTimeBox(JComboBox<Object> timeBox) {
        timeBox.removeAllItems();
        timeBox.addItem("");
        timeBox.addItem("Morning");
        timeBox.addItem("Afternoon");
    }


    /**
     * This function apply a change of the incidence
     * @param incidenceBox the box with the incidence
     */
    public void changeIncidence(JComboBox<Object> incidenceBox) {
        // Getting the new ip, port and incidence
        int incidence = Integer.valueOf(String.valueOf(incidenceBox.getSelectedItem()));

        // Verifying always the same condition and changing the ip, the port and the incidence
        if (incidence != CentralAppController.incidence)
            CentralAppController.incidence = incidence;

        // Changing the ip and the port in the server
        JOptionPane.showMessageDialog(null, "Action realised");
        initBoxIncidence(incidenceBox);
    }


    /**
     * This function change the IP and the Port into the server
     * @param ip the field with the new ip
     * @param port the field with the new port
     */
    public void changeSettings(JTextField ip, JTextField port) {
        // Getting the new ip, port and incidence
        String ipNumber = String.valueOf(ip.getText());
        String portNumber = String.valueOf(port.getText());

        // Verifying always the same condition and changing the ip, the port and the incidence
        if (!ipNumber.equals(""))
            this.ip = ipNumber;
        if (!portNumber.equals(""))
            this.port = Integer.valueOf(portNumber);
        JOptionPane.showMessageDialog(null, "Action realised");
        server.setIp(ipNumber);
        server.setPort(this.port);

        initSettings(ip, port);
    }


    /**
     * This function import some checks from a CSV file
     * @param checksPath the path of the file
     * @return True if the checks have been imported, false else
     */
    public boolean importChecks(JTextField checksPath) {
        String path = checksPath.getText();
        if(path.equals("")) {
            JOptionPane.showMessageDialog(null, "Please enter a path");
            return false;
        }
        else {
            try {
                // Importing all the checks
                company.importChecks(path);
            } catch (IOException e) {
                JOptionPane.showMessageDialog(null, "Import failed");
                return false;
            }
            checksPath.setText("");
            JOptionPane.showMessageDialog(null, "Import realised");
            return true;
        }
    }


    /**
     * This function create a check when the server receive a check from a client (time clock)
     * @param information the information the client sent
     */
    public static void getCheck(String information) {
        if (!information.equals("noinfo")) {
            String[] datas = information.split(",");
            Employee employee = company.searchEmployee(datas[0], datas[1]);
            LocalDate date = LocalDate.of(Integer.valueOf(datas[3].split("-")[0]), Integer.valueOf(datas[3].split("-")[1]), Integer.valueOf(datas[3].split("-")[2]));
            LocalTime time = LocalTime.of(Integer.valueOf(datas[2].split(":")[0]), Integer.valueOf(datas[2].split(":")[1]), 0, 0);

            TheoryCheckHours hours = employee.getCheckHoursDay(date.getDayOfWeek());
            int differenceHours = (hours.getInHour().getHour() * 60 + hours.getInHour().getMinute()) - (hours.getOutHour().getHour() * 60 + hours.getOutHour().getMinute());

            int differenceMorning = (time.getHour() * 60 + time.getMinute()) - (hours.getInHour().getHour() * 60 + hours.getInHour().getMinute());
            int differenceAfternoon = (hours.getOutHour().getHour() * 60 + hours.getOutHour().getMinute()) - (time.getHour() * 60 + time.getMinute());

            // If the employee has normal hours (8h - 18h)
            if (differenceHours <= 0) {
                if (differenceMorning <= differenceAfternoon) {
                    if(employee.getDay(date) == null) {
                        employee.setWorkDaysCheck(time, date);
                        employee.addCountHour(employee.compareTimeMorning(date));
                        updateAll();
                    } else
                        System.out.println("Already Checked");
                }
                else {
                    if (hours.getOutHour().getHour() == 23 && time.getHour() == 0) {
                        if(employee.getDay(LocalDate.ofYearDay(date.getYear(), date.getDayOfYear() - 1)) == null) {
                            employee.setWorkDaysCheck(null, LocalDate.ofYearDay(date.getYear(), date.getDayOfYear() - 1));
                            employee.addAfternoonTimeWorkToday(time);
                            employee.addCountHour(employee.compareTimeAfternoon(LocalDate.ofYearDay(date.getYear(), date.getDayOfYear() - 1)));
                            updateAll();
                        } else if (employee.getDay(LocalDate.ofYearDay(date.getYear(), date.getDayOfYear() - 1)) != null && employee.getDay(LocalDate.ofYearDay(date.getYear(), date.getDayOfYear() - 1)).getRealHourAfternoon() == null){
                            employee.getDay(LocalDate.ofYearDay(date.getYear(), date.getDayOfYear() - 1)).setRealHourAfternoon(time);
                            employee.addCountHour(employee.compareTimeAfternoon(LocalDate.ofYearDay(date.getYear(), date.getDayOfYear() - 1)));
                            updateAll();
                        } else
                            System.out.println("Already Checked");
                    }
                    else {
                        if (employee.getDay(date) == null) {
                            employee.setWorkDaysCheck(null, date);
                            employee.addAfternoonTimeWorkToday(time);
                            employee.addCountHour(employee.compareTimeAfternoon(date));
                            updateAll();
                        } else if (employee.getDay(date) != null && employee.getDay(date).getRealHourAfternoon() == null) {
                            employee.getDay(date).setRealHourAfternoon(time);
                            employee.addCountHour(employee.compareTimeAfternoon(date));
                            updateAll();
                        } else
                            System.out.println("Already Checked");
                    }
                }
            }
            // If the employee works on night
            else {
                if (differenceMorning >= differenceAfternoon) {
                    if(employee.getDay(date) == null) {
                        employee.setWorkDaysCheck(time, date);
                        employee.addCountHour(employee.compareTimeMorning(date));
                        updateAll();
                    } else
                        System.out.println("Already Checked");
                } else {
                    if (hours.getOutHour().getHour() == 0 && time.getHour() == 23) {
                        if (employee.getDay(date) == null) {
                            employee.setWorkDaysCheck(null, date);
                            employee.addAfternoonTimeWorkToday(time);
                            employee.addCountHour(employee.compareTimeAfternoon(date));
                            updateAll();
                        } else if (employee.getDay(date) != null && employee.getDay(date).getRealHourAfternoon() == null) {
                            employee.getDay(date).setRealHourAfternoon(time);
                            employee.addCountHour(employee.compareTimeAfternoon(date));
                            updateAll();
                        } else
                            System.out.println("Already Checked");
                    } else if (date.getDayOfWeek() != DayOfWeek.MONDAY) {
                            hours = employee.getCheckHoursDay(LocalDate.ofYearDay(date.getYear(), date.getDayOfYear() - 1).getDayOfWeek());
                            if (employee.getDay(LocalDate.ofYearDay(date.getYear(), date.getDayOfYear() - 1)) == null) {
                                employee.setWorkDaysCheck(null, LocalDate.ofYearDay(date.getYear(), date.getDayOfYear() - 1));
                                employee.addAfternoonTimeWorkToday(time);
                                int compare = (employee.getDay(LocalDate.ofYearDay(date.getYear(), date.getDayOfYear() - 1)).getRealHourAfternoon().getHour() * 60 + employee.getDay(LocalDate.ofYearDay(date.getYear(), date.getDayOfYear() - 1)).getRealHourAfternoon().getMinute())
                                        - (hours.getOutHour().getHour() * 60 + hours.getOutHour().getMinute());
                                employee.addCountHour(-compare);
                                updateAll();
                            } else if (employee.getDay(LocalDate.ofYearDay(date.getYear(), date.getDayOfYear() - 1)) != null && employee.getDay(LocalDate.ofYearDay(date.getYear(), date.getDayOfYear() - 1)).getRealHourAfternoon() == null) {
                                employee.getDay(LocalDate.ofYearDay(date.getYear(), date.getDayOfYear() - 1)).setRealHourAfternoon(time);
                                int compare = (employee.getDay(LocalDate.ofYearDay(date.getYear(), date.getDayOfYear() - 1)).getRealHourAfternoon().getHour() * 60 + employee.getDay(LocalDate.ofYearDay(date.getYear(), date.getDayOfYear() - 1)).getRealHourAfternoon().getMinute())
                                        - (hours.getOutHour().getHour() * 60 + hours.getOutHour().getMinute());
                                employee.addCountHour(-compare);
                                updateAll();
                            } else
                                System.out.println("Already Checked");
                        } else
                            System.out.println("Cannot check on Monday morning, he didn't work on sunday");
                }
            }
        }
    }


    /**
     * This function create a new JFrame with information to add a check
     * @param checkView the frame where there's all the components
     * @param employeesTable the table which contains the information about the employees
     * @param checkTable teh table which contains all the checks
     */
    public void addCheck(AddCheckFrame checkView, JTable employeesTable, JTable checkTable) {
        // Creating the view
        initEmployeeBox(checkView.getEmployeeBox());

        // Creating the listener when the user change the employee selected in the box to update the box with the days
        checkView.getEmployeeBox().addItemListener(e -> initDateBoxPerEmployee(checkView.getDateBox(), checkView.getEmployeeBox().getSelectedItem()));

        // Init the JComboBox with the morning and the afternoon
        JComboBox<Object> timeBox = checkView.getTimeBox();
        timeBox.removeAllItems();
        timeBox.addItem("");
        timeBox.addItem("Morning");
        timeBox.addItem("Afternoon");

        // Init the JComboBox with the hour and the minute
        initHoursBoxes(checkView.getHourBox(), checkView.getMinuteBox());

        // Creating the listener of the button to add the manual check
        checkView.getAddCheckButton().addActionListener(e -> {
            // Calling the next function (under it)
            actionAddCheck(checkView.getEmployeeBox(), checkView.getDateBox(), checkView.getHourBox(), checkView.getMinuteBox(), checkView.getTimeBox());

            // Updating the tables
            initEmployeesTable(employeesTable);
            initTableChecks(checkTable);
        });

        // Creating the frame
        openFrame(checkView);
    }


    /**
     * This function add a missing check
     * @param employeeBox the box with the employee we're going to add a check from
     * @param dateBox the box with the date we forgot to check
     * @param hourBox the box he arrived in the company
     * @param minuteBox the minutes he arrived int the company
     * @param timeBox the time he arrived, morning or afternoon
     */
    public void actionAddCheck(JComboBox employeeBox, JComboBox dateBox, JComboBox hourBox, JComboBox minuteBox, JComboBox timeBox) {
        // Getting the information of the check the user want to add
        String[] employeeNames = String.valueOf(employeeBox.getSelectedItem()).split(" ");
        String[] date = String.valueOf(dateBox.getSelectedItem()).split("-");
        String hour = String.valueOf(hourBox.getSelectedItem());
        String minute = String.valueOf(minuteBox.getSelectedItem());
        String time = String.valueOf(timeBox.getSelectedItem());

        // Verifying if all these information are not equals to "nothing"
        if (String.valueOf(employeeBox.getSelectedItem()).equals(""))
            JOptionPane.showMessageDialog(null, "Please select an employee");
        else if (String.valueOf(dateBox.getSelectedItem()).equals(""))
            JOptionPane.showMessageDialog(null, "Please select a date");
        else if (hour.equals("") || minute.equals(""))
            JOptionPane.showMessageDialog(null, "Please select an hour and some minutes");
        else if (time.equals(""))
            JOptionPane.showMessageDialog(null, "Please select morning or afternoon");

            // Adding the check
        else {
            // Searching for the employee and getting the date
           Employee employee = company.searchEmployee(employeeNames[0], employeeNames[1]);
            int[] dating = {Integer.valueOf(date[0]), Integer.valueOf(date[1]), Integer.valueOf(date[2])};
            LocalDate localDate = LocalDate.of(dating[0], dating[1], dating[2]);


            for (WorkToday workToday : employee.getWorkDaysCheck()) {
                // Verifying for each workToday if the date correspond to the date we want to add a missing check
                if (workToday.getDate().equals(localDate)) {
                    // If we want to add the morning
                    if(time.equals("Morning")) {
                        TheoryCheckHours hoursDay = employee.getCheckHoursDay(workToday.getDate().getDayOfWeek());
                        // Verifying if the employee checked or not
                        if (workToday.getRealHourMorning() != null) {
                            JOptionPane.showMessageDialog(null, "The employee checked, or you had already add a manual check");
                            employeeBox.setSelectedItem("");
                            timeBox.setSelectedItem("");
                            hourBox.setSelectedItem("");
                            minuteBox.setSelectedItem("");
                            dateBox.setSelectedItem("");
                        }
                        else {
                            // Setting the hour and adding the late time to his count hour
                            workToday.setRealHourMorning(LocalTime.of(Integer.valueOf(hour), Integer.valueOf(minute)));
                            employee.addCountHour(employee.compareTimeMorning(workToday.getDate()));
                            employeeBox.setSelectedItem("");
                            timeBox.setSelectedItem("");
                            hourBox.setSelectedItem("");
                            minuteBox.setSelectedItem("");
                            dateBox.setSelectedItem("");
                            JOptionPane.showMessageDialog(null, "Check added");
                        }
                    }
                    // If we want to add the afternoon
                    else if (time.equals("Afternoon")) {
                        TheoryCheckHours hoursDay = employee.getCheckHoursDay(workToday.getDate().getDayOfWeek());
                        // Verifying if the employee checked or not
                        if(workToday.getRealHourAfternoon() == null) {
                            // Setting the hour and adding the late time to his count hour
                            workToday.setRealHourAfternoon(LocalTime.of(Integer.valueOf(hour), Integer.valueOf(minute)));
                            employee.addCountHour(employee.compareTimeAfternoon(workToday.getDate()));
                            employeeBox.setSelectedItem("");
                            timeBox.setSelectedItem("");
                            hourBox.setSelectedItem("");
                            minuteBox.setSelectedItem("");
                            dateBox.setSelectedItem("");
                            JOptionPane.showMessageDialog(null, "Check added");
                        }
                        else {
                            JOptionPane.showMessageDialog(null, "The employee checked, or you had already add a manual check");
                            employeeBox.setSelectedItem("");
                            timeBox.setSelectedItem("");
                            hourBox.setSelectedItem("");
                            minuteBox.setSelectedItem("");
                            dateBox.setSelectedItem("");
                        }
                    }
                    break;
                }
            }

        }
    }


    /**
     * This function import some employees from a CSV file
     * @param fileName the path of the file
     * @return True is the import succeeded, else false
     */
    public boolean importCSV(JTextField fileName) {
        if (!String.valueOf(fileName.getText()).equals("")) {
            try {
                company.importCSV(String.valueOf(fileName.getText()));
                JOptionPane.showMessageDialog(null,"File successfully imported", "Success", JOptionPane.INFORMATION_MESSAGE);
                fileName.setText("");
                syncEmployees();
                TCPClient client = new TCPClient("localhost", 8090);
                String information = "";
                for (Employee employee1 : company.getEmployees())
                    information = information + employee1.getIdEmployee() + "," + employee1.getFirstName() + "," + employee1.getLastName() + ";";
                client.setInformation(information);
                client.run();
                return true;
            } catch (IOException e) {
                JOptionPane.showMessageDialog(null, "Import failed" , "Error", JOptionPane.ERROR_MESSAGE);
                fileName.setText("");
                return false;
            }
        }
        else {
            JOptionPane.showMessageDialog(null, "Please enter a path", "Warning", JOptionPane.WARNING_MESSAGE);
            return false;
        }
    }


    /**
     * This function export all the employees into a CSV file
     * @param fileName the path of the file
     * @return True if the export succeeded, else false
     */
    public boolean exportCSV(JTextField fileName) {
        if (!String.valueOf(fileName.getText()).equals("")) {
            try {
                company.exportCSV(String.valueOf(fileName.getText()));
                JOptionPane.showMessageDialog(null,"File successfully exported", "Success", JOptionPane.INFORMATION_MESSAGE);
                fileName.setText("");
                return true;
            } catch (IOException e) {
                JOptionPane.showMessageDialog(null, "Export failed" , "Error", JOptionPane.ERROR_MESSAGE);
                fileName.setText("");
                return false;
            }
        }
        else {
            JOptionPane.showMessageDialog(null, "Please enter a path", "Warning", JOptionPane.INFORMATION_MESSAGE);
            return false;
        }
    }


    /**
     * This function is the action when we want to create an employee
     * @param firstName the first name of the employee
     * @param lastName the last name of the employee
     * @param email the mail of the employee
     * @param morningHour ComboBox containing the hour of the morning
     * @param morningMinute ComboBox containing the minute of the morning
     * @param afternoonHour ComboBox containing the hour of the afternoon
     * @param afternoonMinute ComboBox containing the minute of the afternoon
     * @param employeeButton Button which indicate if we are creating an employee
     * @param managerButton Button which indicate if we are creating a manager
     */
    public void createEmployee(JTextField firstName, JTextField lastName, JTextField email, JComboBox<Object> morningHour, JComboBox<Object> afternoonHour,
                               JComboBox<Object> morningMinute, JComboBox<Object> afternoonMinute,JRadioButton employeeButton, JRadioButton managerButton) {
        // Getting the text from JTextField
        String first = firstName.getText().trim();
        String last = lastName.getText().trim();
        String mail = email.getText().trim();

        // Verifying if the text are different of "nothing"
        if (first.equals(""))
            JOptionPane.showMessageDialog(null,"Please enter a first name");
        else if (last.equals(""))
            JOptionPane.showMessageDialog(null,"Please enter a last name");
        else if (mail.equals(""))
            JOptionPane.showMessageDialog(null,"Please enter an email");

            // Verifying if an employee with that names already exists
        else if (company.searchEmployee(first, last) != null)
            JOptionPane.showMessageDialog(null,"An employee with that first and last name already exists");

            // Getting the radios button
        else if(employeeButton.isSelected()){ // If the user want to create an employee
            // Verifying if the user chose hours of beginning and ending
            if (String.valueOf(morningHour.getSelectedItem()).equals("") || String.valueOf(morningMinute.getSelectedItem()).equals("") ||
                    String.valueOf(afternoonHour.getSelectedItem()).equals("") || String.valueOf(afternoonMinute.getSelectedItem()).equals(""))
                JOptionPane.showMessageDialog(null,"One of the hours or minutes has not been selected");
                // Finally creating the employee
            else {
                Employee employee = new Employee(first, last, mail);

                // Setting his hours
                for (DayOfWeek day : DayOfWeek.values()) {
                    if (day != DayOfWeek.SUNDAY && day != DayOfWeek.SATURDAY) {
                        employee.setTheoryCheckHours(day, LocalTime.of(Integer.valueOf(String.valueOf(morningHour.getSelectedItem())), Integer.valueOf(String.valueOf(morningMinute.getSelectedItem()))),
                                LocalTime.of(Integer.valueOf(String.valueOf(afternoonHour.getSelectedItem())), Integer.valueOf(String.valueOf(afternoonMinute.getSelectedItem()))));
                    }
                }
                // Adding him to the company
                company.addEmployee(employee);

                // Setting back all the Component to their base
                firstName.setText("");
                lastName.setText("");
                email.setText("");
                initButtonEmployeeTime(employeeButton, managerButton);
                initHoursBoxes(morningHour, morningMinute);
                initHoursBoxes(afternoonHour, afternoonMinute);
                JOptionPane.showMessageDialog(null,"Employee successfully created");
                syncEmployees();
                TCPClient client = new TCPClient("localhost", 8090);
                String information = "";
                for (Employee employee1 : company.getEmployees())
                    information = information + employee1.getIdEmployee() + "," + employee1.getFirstName() + "," + employee1.getLastName() + ";";
                client.setInformation(information);
                client.run();
            }
        }
        else if (managerButton.isSelected()){ // If the user want to create a manager
            // Verifying if the user chose hours of beginning and ending
            if (String.valueOf(morningHour.getSelectedItem()).equals("") || String.valueOf(morningMinute.getSelectedItem()).equals("") ||
                    String.valueOf(afternoonHour.getSelectedItem()).equals("") || String.valueOf(afternoonMinute.getSelectedItem()).equals(""))
                JOptionPane.showMessageDialog(null,"One of the hours or minutes has not been selected");
                // Finally creating the manager
            else {
                Manager employee = new Manager(first, last, mail);

                // Setting his hours
                for (DayOfWeek day : DayOfWeek.values()) {
                    if (day != DayOfWeek.SUNDAY && day != DayOfWeek.SATURDAY) {
                        employee.setTheoryCheckHours(day, LocalTime.of(Integer.valueOf(String.valueOf(morningHour.getSelectedItem())), Integer.valueOf(String.valueOf(morningMinute.getSelectedItem()))),
                                LocalTime.of(Integer.valueOf(String.valueOf(afternoonHour.getSelectedItem())), Integer.valueOf(String.valueOf(afternoonMinute.getSelectedItem()))));
                    }
                }
                // Adding him to the company
                company.addEmployee(employee);

                // Setting back all the Component to their base
                firstName.setText("");
                lastName.setText("");
                email.setText("");
                initButtonEmployeeTime(employeeButton, managerButton);
                initHoursBoxes(morningHour, morningMinute);
                initHoursBoxes(afternoonHour, afternoonMinute);
                JOptionPane.showMessageDialog(null,"Manager successfully created");
                syncEmployees();
                TCPClient client = new TCPClient("localhost", 8090);
                String information = "";
                for (Employee employee1 : company.getEmployees())
                    information = information + employee1.getIdEmployee() + "," + employee1.getFirstName() + "," + employee1.getLastName() + ";";
                client.setInformation(information);
                client.run();
            }
        }

        // The user didn't choose a type to his employee
        else {
            JOptionPane.showMessageDialog(null,"You haven't choose if it's an employee or a manager");
        }
    }


    /**
     * This function is used to modify an employee, fields can be null if we want to change just the hours, the same hours can be null we want to change just the names
     * @param modifyEmployeeBox the box with the employees
     * @param firstName the field which contains the new first name
     * @param lastName the field which contains the new last name
     * @param mail the field which contains the new mail
     * @param day the comboBox with the day
     * @param morningHour the new hour of morning
     * @param morningMinute the new minutes of morning
     * @param afternoonHour the new hour of afternoon
     * @param afternoonMinute the new minutes of afternoon
     * @param departmentName the name of the department if we want to add the employee to it
     */
    public void modifyEmployee(JComboBox modifyEmployeeBox, JTextField firstName, JTextField lastName, JTextField mail, JComboBox day,
                               JComboBox<Object> morningHour, JComboBox<Object> morningMinute, JComboBox<Object> afternoonHour, JComboBox<Object> afternoonMinute, JComboBox departmentName) {
        boolean changed = false;
        // Verifying if an employee has been chosen
        if (String.valueOf(modifyEmployeeBox.getSelectedItem()).equals(""))
            JOptionPane.showMessageDialog(null,"You haven't choose an employee or a manager");
        else {
            // Getting the employee
            String[] names = String.valueOf(modifyEmployeeBox.getSelectedItem()).split(" ");
            Employee employee = company.searchEmployee(names[0], names[1]);

            // Changing the first name
            if (!firstName.getText().equals("") && lastName.getText().equals("")) {
                employee.setFirstName(firstName.getText().trim());
                firstName.setText("");
                initEmployeeBox(modifyEmployeeBox);
                changed = true;
            }
            // Changing the last name
            else if (!lastName.getText().equals("") && firstName.getText().equals("")) {
                employee.setLastName(lastName.getText().trim());
                lastName.setText("");
                initEmployeeBox(modifyEmployeeBox);
                changed = true;
            }
            else if (!lastName.getText().equals("") && !firstName.getText().equals("")){
                if (company.searchEmployee(firstName.getText(), lastName.getText()) == null) {
                    employee.setFirstName(firstName.getText().trim());
                    firstName.setText("");
                    employee.setLastName(lastName.getText().trim());
                    lastName.setText("");
                    initEmployeeBox(modifyEmployeeBox);
                    changed = true;
                }
                else
                    JOptionPane.showMessageDialog(null, "An employee with these names already exists", "Error", JOptionPane.ERROR_MESSAGE);
            }

            // Changing the mail
            if (!mail.getText().equals("")) {
                employee.setEmail(mail.getText().trim());
                mail.setText("");
                initEmployeeBox(modifyEmployeeBox);
                changed = true;
            }

            // Verifying if a day and hours has been chosen
            if (String.valueOf(day.getSelectedItem()).equals("") && (!String.valueOf(morningHour.getSelectedItem()).equals("") || !String.valueOf(morningMinute.getSelectedItem()).equals("") ||
                    !String.valueOf(afternoonHour.getSelectedItem()).equals("") || !String.valueOf(afternoonMinute.getSelectedItem()).equals("")))
                JOptionPane.showMessageDialog(null,"You haven't choose a day", "Warning", JOptionPane.WARNING_MESSAGE);
                // If it's the case, we change the datas of our employee
            else if (!String.valueOf(day.getSelectedItem()).equals("")) {

                // If the user want to change the hours of the morning
                if (!String.valueOf(morningHour.getSelectedItem()).equals("") && !String.valueOf(morningMinute.getSelectedItem()).equals(""))
                {
                    DayOfWeek dayOfWeek = DayOfWeek.valueOf(String.valueOf(day.getSelectedItem()));
                    int hourM = Integer.valueOf(String.valueOf(morningHour.getSelectedItem()));
                    int minuteM = Integer.valueOf(String.valueOf(morningMinute.getSelectedItem()));
                    employee.setTheoryCheckHours(dayOfWeek, LocalTime.of(hourM,minuteM), employee.getCheckHoursDay(dayOfWeek).getOutHour());
                    initHoursBoxes(morningHour, morningMinute);
                    initEmployeeBox(modifyEmployeeBox);
                    changed = true;
                }

                // If the user want to change the hours of the afternoon
                if (!String.valueOf(afternoonHour.getSelectedItem()).equals("") && !String.valueOf(afternoonMinute.getSelectedItem()).equals(""))
                {
                    DayOfWeek dayOfWeek = DayOfWeek.valueOf(String.valueOf(day.getSelectedItem()));
                    int hourA = Integer.valueOf(String.valueOf(afternoonHour.getSelectedItem()));
                    int minuteA = Integer.valueOf(String.valueOf(afternoonMinute.getSelectedItem()));
                    employee.setTheoryCheckHours(dayOfWeek, employee.getCheckHoursDay(dayOfWeek).getInHour(), LocalTime.of(hourA,minuteA));
                    initHoursBoxes(afternoonHour, afternoonMinute);
                    initEmployeeBox(modifyEmployeeBox);
                    changed = true;
                }
                else
                    JOptionPane.showMessageDialog(null,"You haven't choose an hour and minutes on afternoon or morning, so hours of begin and end have not been changed", "Warning", JOptionPane.WARNING_MESSAGE);
                initEmployeeBoxDays(day);
            }

            // Changing the department of the employee
            if (!String.valueOf(departmentName.getSelectedItem()).equals("")) {
                // Verifying if it's not a manager which is managing a department, or if he's an employee or if he's without department
                if (employee.getIdDepartment() == -1 || employee.getClass().getSimpleName().equals("Employee") || (employee.getClass().getSimpleName().equals("Manager") && !company.isManaging(employee))) {
                    String name = String.valueOf(departmentName.getSelectedItem());
                    Department department = company.searchDepartment(name);
                    company.changeDepartmentOfEmployee(department, employee);
                    initDepartmentBox(departmentName);
                    initEmployeeBox(modifyEmployeeBox);
                    changed = true;
                }
                else
                    JOptionPane.showMessageDialog(null,"He's a manager managing a department, can't be moved", "Error", JOptionPane.ERROR_MESSAGE);
            }

            // Synchronizing the list of employees
            if (changed) {
                JOptionPane.showMessageDialog(null, "Employee Modified", "Information", JOptionPane.INFORMATION_MESSAGE);
                syncEmployees();
                TCPClient client = new TCPClient("localhost", 8090);
                String information = "";
                for (Employee employee1 : company.getEmployees())
                    information = information + employee1.getIdEmployee() + "," + employee1.getFirstName() + "," + employee1.getLastName() + ";";
                client.setInformation(information);
                client.run();
            }
            else
                JOptionPane.showMessageDialog(null, "Nothing changed", "Information", JOptionPane.INFORMATION_MESSAGE);
        }
    }


    /**
     * This function delete an employee from the company
     * @param deleteEmployeeBox the box where there's the first and the last name of the employee
     */
    public void deleteEmployee(JComboBox<Object> deleteEmployeeBox){
        // Verifying if the user chose an employee
        if (String.valueOf(deleteEmployeeBox.getSelectedItem()).equals(""))
            JOptionPane.showMessageDialog(null,"You haven't choose an employee or a manager", "Warning", JOptionPane.WARNING_MESSAGE);
        else {
            // Getting the names
            String employeeNames = String.valueOf(deleteEmployeeBox.getSelectedItem());
            String[] names = employeeNames.split(" ");

            // Verifying if the user really want to delete him
            int answer = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete him ?",
                    "Confirmation", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            if (answer==JOptionPane.YES_OPTION) {
                // Deleting the employee
                company.deleteEmployee(names[0], names[1]);
                initEmployeeBox(deleteEmployeeBox);
                JOptionPane.showMessageDialog(null, "Employee or Manager successfully deleted", "Information", JOptionPane.INFORMATION_MESSAGE);
                syncEmployees();
                TCPClient client = new TCPClient("localhost", 8090);
                String information = "";
                for (Employee employee1 : company.getEmployees())
                    information = information + employee1.getIdEmployee() + "," + employee1.getFirstName() + "," + employee1.getLastName() + ";";
                client.setInformation(information);
                client.run();
            }
        }
    }


    /**
     * This function create a department
     * @param managerBox the box where there's the first and last name of the new manager
     * @param departmentName the department's name
     */
    public void createDepartment(JComboBox<Object> managerBox, JTextField departmentName) {
        // Getting the name of the new department
        String name = String.valueOf(departmentName.getText().trim());

        // Verifying if the department doesn't already exists
        if (company.searchDepartment(name) != null)
            JOptionPane.showMessageDialog(null,"A department with this name already exists", "Error", JOptionPane.ERROR_MESSAGE);
        else {
            // Verifying if the name is not equal to "nothing"
            if (String.valueOf(managerBox.getSelectedItem()).equals(""))
                JOptionPane.showMessageDialog(null,"Please select a manager for the creation of the department", "Warning", JOptionPane.WARNING_MESSAGE);
            else {
                // Getting the future manager
                String[] names = String.valueOf(managerBox.getSelectedItem()).split(" ");

                // Creating the department
                Department department = new Department(name);

                // Searching the manager or employee who is going to manage the department
                Employee employee = company.searchEmployee(names[0], names[1]);

                // Creating the manager in all case, even if he was already one and adding him to the department and the company,
                // also deleting him from the company first
                company.deleteEmployee(names[0], names[1]);
                Manager manager = new Manager(employee);
                department.setManager(manager);
                company.addEmployee(manager);
                company.addDepartments(department);
                departmentName.setText("");
                initManagerBox(managerBox);
                JOptionPane.showMessageDialog(null, "Department created", "Information", JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }


    /**
     * This function modify the name of a department
     * @param departmentBox the box where there's the department the user want to change the name
     * @param departmentName the field where there's the new department's name
     */
    public void modifyDepartment(JComboBox<Object> departmentBox, JTextField departmentName) {
        // Verifying if a department has been chosen
        if(String.valueOf(departmentBox.getSelectedItem()).equals(""))
            JOptionPane.showMessageDialog(null,"You haven't choose a department");
        else {
            // Verifying if the new name isn't "nothing"
            if (departmentName.getText().trim().equals(""))
                JOptionPane.showMessageDialog(null,"You want to modify the department but there's no new name", "Warning", JOptionPane.WARNING_MESSAGE);
            else {
                // Searching the department
                Department department = company.searchDepartment(String.valueOf(departmentBox.getSelectedItem()).trim());
                if (company.searchDepartment(departmentName.getText()) == null) {
                    department.setDepartmentName(departmentName.getText()); // Changing the name
                    departmentName.setText("");
                    initDepartmentBox(departmentBox);
                    JOptionPane.showMessageDialog(null,"Department successfully modified");
                }
                else
                    JOptionPane.showMessageDialog(null, "A department with that name already exists", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }


    /**
     * This function delete a department from the company
     * @param departmentBox the box where there's the department to delete
     */
    public void deleteDepartment(JComboBox<Object> departmentBox) {
        // Verifying if the use chose a department to delete
        if (String.valueOf(departmentBox.getSelectedItem()).equals(""))
            JOptionPane.showMessageDialog(null,"You haven't choose a department", "Warning", JOptionPane.WARNING_MESSAGE);
        else {
            // Verifying if the user really want to delete it
            int answer = JOptionPane.showConfirmDialog(null,
                    "Are you sure you want to delete it ?",
                    "Confirmation",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE);
            if (answer == JOptionPane.YES_OPTION) {
                // Deleting the department
                String departmentName = String.valueOf(departmentBox.getSelectedItem());
                company.deleteDepartment(String.valueOf(departmentName));
                initDepartmentBox(departmentBox);
                JOptionPane.showMessageDialog(null, "Department successfully deleted");
            }
        }
    }

    /**
     * This function create a frame with the checks of all employees from the company or from a department with a date or not
     * @param departmentBox the box which contains the department's name
     * @param dateBox the box which contains the date
     * @param table the table to update
     */
    public void filtrateCheckInOut(JComboBox<Object> departmentBox, JComboBox<Object> dateBox, JTable table) {
        if(String.valueOf(departmentBox.getSelectedItem()).equals("") && (String.valueOf(dateBox.getSelectedItem()).equals("All Checks") || String.valueOf(dateBox.getSelectedItem()).equals("")))
            initTableChecks(table);
        else if(!String.valueOf(departmentBox.getSelectedItem()).equals("") && String.valueOf(dateBox.getSelectedItem()).equals(""))
            filtrateCheckInOutDepartment(departmentBox, table);
        else if(String.valueOf(departmentBox.getSelectedItem()).equals("") && !String.valueOf(dateBox.getSelectedItem()).equals("") && !String.valueOf(dateBox.getSelectedItem()).equals("All Checks"))
            filtrateCheckInOutDate(dateBox, table);
        else if (!String.valueOf(departmentBox.getSelectedItem()).equals("") && !String.valueOf(dateBox.getSelectedItem()).equals("") && !String.valueOf(dateBox.getSelectedItem()).equals("All Checks"))
            filtrateCheckInOutDepartmentAndDate(departmentBox, dateBox, table);
        else if (!String.valueOf(departmentBox.getSelectedItem()).equals("") && String.valueOf(dateBox.getSelectedItem()).equals("All Checks"))
            filtrateCheckInOutDepartment(departmentBox, table);
    }


    /**
     * This function create a frame with the checks of all employees from a department with a date
     * @param departmentBox the box which contains the department's name
     * @param dateBox the box which contains the date
     * @param table the table to update
     */
    private void filtrateCheckInOutDepartmentAndDate(JComboBox<Object> departmentBox, JComboBox<Object> dateBox, JTable table) {
        // Getting the department and the date
        Department department = company.searchDepartment(String.valueOf(departmentBox.getSelectedItem()));
        String[] dating = String.valueOf(dateBox.getSelectedItem()).split("-");
        LocalDate date = LocalDate.of(Integer.valueOf(dating[0]), Integer.valueOf(dating[1]), Integer.valueOf(dating[2]));

        // Setting the datas of the table
        String[] columns = {
                "First name",
                "Last name",
                "Count hour",
                "Date",
                "On Morning",
                "On Afternoon",
        };

        // The size of the table if the number of employees, because an employee can't check to time on morning or afternoon on the same day
        Object[][] datas = new Object[department.getEmployees().size()][6];
        int index = 0;
        for (Employee employee : department.getEmployees()) {
            for (WorkToday workToday : employee.getWorkDaysCheck()) {
                if (date.equals(workToday.getDate())) {
                    datas[index][0] = employee.getFirstName();
                    datas[index][1] = employee.getLastName();
                    datas[index][2] = employee.getCountHour();
                    datas[index][3] = workToday.getDate();
                    if (workToday.getRealHourMorning() == null)
                        datas[index][4] = "null";
                    else
                        datas[index][4] = workToday.getRealHourMorning() + " " + (employee.compareTimeMorning(workToday.getDate()) >= incidence ? employee.compareTimeMorning(workToday.getDate()) : "");
                    if (workToday.getRealHourAfternoon() == null)
                        datas[index][5] = "null";
                    else
                        datas[index][5] = workToday.getRealHourAfternoon() + " " + (employee.compareTimeAfternoon(workToday.getDate()) >= incidence ? employee.compareTimeAfternoon(workToday.getDate()) : "");
                    index++;
                }
            }
        }

        // Adding to the table the check of the manager on this day
        for (WorkToday workToday : department.getManager().getWorkDaysCheck()) {
            if (date.equals(workToday.getDate())) {
                datas[index][0] = department.getManager().getFirstName();
                datas[index][0] = department.getManager().getLastName();
                datas[index][0] = department.getManager().getCountHour();
                datas[index][3] = workToday.getDate();
                if (workToday.getRealHourMorning() == null)
                    datas[index][4] = "null";
                else
                    datas[index][4] = workToday.getRealHourMorning() + " " + (department.getManager().compareTimeMorning(workToday.getDate()) >= incidence ? department.getManager().compareTimeMorning(workToday.getDate()) : "");
                if (workToday.getRealHourAfternoon() == null)
                    datas[index][5] = "null";
                else
                    datas[index][5] = workToday.getRealHourAfternoon() + " " + (department.getManager().compareTimeAfternoon(workToday.getDate()) >= incidence ? department.getManager().compareTimeAfternoon(workToday.getDate()) : "");
                index++;
            }
        }

        while (index < datas.length) {
            datas[index][0] = "";
            index++;
        }

        DefaultTableModel model = new DefaultTableModel(datas, columns) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        for (int i = model.getRowCount() - 1; i >= 0 ; i--) {
            if (String.valueOf(model.getValueAt(i,0)).equals(""))
                model.removeRow(i);
        }

        table.setModel(model);

        table.setAutoCreateRowSorter(true);

        RowSorter.SortKey[] sks = new RowSorter.SortKey[]{
                new RowSorter.SortKey(3, SortOrder.DESCENDING)
        };
        table.getRowSorter().setSortKeys(Arrays.asList(sks));
        for(int i = 0; i < columns.length; i++) {
            ((DefaultRowSorter)table.getRowSorter()).setSortable(i,false);
        }

        initDepartmentBox(departmentBox);
        initDateBox(dateBox);
    }


    /**
     * This function create a frame with the checks of all employees from a department
     * @param departmentBox the box which contains the department's name
     * @param table the table to update
     */
    private void filtrateCheckInOutDepartment(JComboBox<Object> departmentBox, JTable table) {
        // Getting the department
        Department department = company.searchDepartment(String.valueOf(departmentBox.getSelectedItem()));

        // Creating the table with all the checks of all employees inside the department
        String[] columns = {
                "First name",
                "Last name",
                "Count hour",
                "Date",
                "On Morning",
                "On Afternoon",
        };

        // The size of the datas is the number of the employees by the size of the biggest WorkToday's size list of the employees
        Object[][] datas = new Object[(department.getEmployees().size() + 1) * department.getBiggestSizeWorkTodaysList()][6];
        int index = 0;
        for (Employee employee : department.getEmployees()) {
            for (WorkToday workToday : employee.getWorkDaysCheck()) {
                datas[index][0] = employee.getFirstName();
                datas[index][1] = employee.getLastName();
                datas[index][2] = employee.getCountHour();
                datas[index][3] = workToday.getDate();
                if (workToday.getRealHourMorning() == null)
                    datas[index][4] = "null";
                else
                    datas[index][4] = workToday.getRealHourMorning() + " " + (employee.compareTimeMorning(workToday.getDate()) >= incidence ? employee.compareTimeMorning(workToday.getDate()) : "");
                if (workToday.getRealHourAfternoon() == null)
                    datas[index][5] = "null";
                else
                    datas[index][5] = workToday.getRealHourAfternoon() + " " + (employee.compareTimeAfternoon(workToday.getDate()) >= incidence ? employee.compareTimeAfternoon(workToday.getDate()) : "");
                index++;
            }
        }

        // Adding to the table the checks of the manager
        for (WorkToday workToday : department.getManager().getWorkDaysCheck()) {
            datas[index][0] = department.getManager().getFirstName();
            datas[index][0] = department.getManager().getLastName();
            datas[index][0] = department.getManager().getCountHour();
            datas[index][3] = workToday.getDate();
            if (workToday.getRealHourMorning() == null)
                datas[index][4] = "null";
            else
                datas[index][4] = workToday.getRealHourMorning() + " " + (department.getManager().compareTimeMorning(workToday.getDate()) >= incidence ? department.getManager().compareTimeMorning(workToday.getDate()) : "");
            if (workToday.getRealHourAfternoon() == null)
                datas[index][5] = "null";
            else
                datas[index][5] = workToday.getRealHourAfternoon() + " " + (department.getManager().compareTimeAfternoon(workToday.getDate()) >= incidence ? department.getManager().compareTimeAfternoon(workToday.getDate()) : "");
            index++;
        }

        while (index < datas.length) {
            datas[index][0] = "";
            index++;
        }

        DefaultTableModel model = new DefaultTableModel(datas, columns) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        for (int i = model.getRowCount() - 1; i >= 0 ; i--) {
            if (String.valueOf(model.getValueAt(i,0)).equals(""))
                model.removeRow(i);
        }

        // Creating the frame
        table.setModel(model);

        table.setAutoCreateRowSorter(true);

        RowSorter.SortKey[] sks = new RowSorter.SortKey[]{
                new RowSorter.SortKey(3, SortOrder.DESCENDING)
        };
        table.getRowSorter().setSortKeys(Arrays.asList(sks));
        for(int i = 0; i < columns.length; i++) {
            if (i != 3)
                ((DefaultRowSorter)table.getRowSorter()).setSortable(i,false);
        }

        initDepartmentBox(departmentBox);
    }


    /**
     * This function create a frame with the checks of all employees from the company with a date
     * @param dateBox the box which contains the date
     * @param table the table to update
     */
    private void filtrateCheckInOutDate(JComboBox<Object> dateBox, JTable table) {
        // Getting the date
        String[] dating = String.valueOf(dateBox.getSelectedItem()).split("-");
        LocalDate date = LocalDate.of(Integer.valueOf(dating[0]), Integer.valueOf(dating[1]), Integer.valueOf(dating[2]));

        // Setting the datas of the table
        String[] columns = {
                "First name",
                "Last name",
                "Count hour",
                "Date",
                "On Morning",
                "On Afternoon",
        };

        // The size of the datas is the number of the employees by the size of the biggest WorkToday's size list of the employees
        Object[][] datas = new Object[company.getEmployees().size() * company.getBiggestSizeWorkTodayList()][6];
        int index = 0;
        for (Employee employee : company.getEmployees()) {
            for (WorkToday workToday : employee.getWorkDaysCheck()) {
                if (date.equals(workToday.getDate())) {
                    datas[index][0] = employee.getFirstName();
                    datas[index][1] = employee.getLastName();
                    datas[index][2] = employee.getCountHour();
                    datas[index][3] = workToday.getDate();
                    if (workToday.getRealHourMorning() == null)
                        datas[index][4] = "null";
                    else
                        datas[index][4] = workToday.getRealHourMorning() + " " + (employee.compareTimeMorning(workToday.getDate()) >= incidence ? employee.compareTimeMorning(workToday.getDate()) : "");
                    if (workToday.getRealHourAfternoon() == null)
                        datas[index][5] = "null";
                    else
                        datas[index][5] = workToday.getRealHourAfternoon() + " " + (employee.compareTimeAfternoon(workToday.getDate()) >= incidence ? employee.compareTimeAfternoon(workToday.getDate()) : "");
                    index++;
                }
            }
        }

        while (index < datas.length) {
            datas[index][0] = "";
            index++;
        }

        DefaultTableModel model = new DefaultTableModel(datas, columns) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        for (int i = model.getRowCount() - 1; i >= 0 ; i--) {
            if (String.valueOf(model.getValueAt(i,0)).equals(""))
                model.removeRow(i);
        }

        table.setModel(model);

        table.setAutoCreateRowSorter(true);

        RowSorter.SortKey[] sks = new RowSorter.SortKey[]{
                new RowSorter.SortKey(3, SortOrder.DESCENDING)
        };
        table.getRowSorter().setSortKeys(Arrays.asList(sks));
        for(int i = 0; i < columns.length; i++) {
            ((DefaultRowSorter)table.getRowSorter()).setSortable(i,false);
        }

        initDateBox(dateBox);
    }


    /**
     * This function init the table with all the employees
     * @param employeesTable the table where we are going to modify the model
     */
    public static void initEmployeesTable(JTable employeesTable) {
        // Setting the columns of the table
        String[] columns = {
                "First name",
                "Last name",
                "Email",
                "ID",
                "Time Late",
                "Department ID"
        };

        // Setting the datas
        String[][] datas = new String[company.getEmployees().size()][100];
        int index = 0;
        for (Employee employee : company.getEmployees()) {
            datas[index] = employee.toString().split(",");
            index++;
        }

        // Setting the model of the table
        employeesTable.setModel(new DefaultTableModel(datas, columns) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        });

        employeesTable.setAutoCreateRowSorter(true);
        for(int i = 0; i < columns.length; i++) {
            ((DefaultRowSorter)employeesTable.getRowSorter()).setSortable(i,false);
        }
    }


    /**
     * This function init the table with all the checks
     * @param tableChecks the table we are going to change the model
     */
    public static void initTableChecks(JTable tableChecks) {
        String[] columns = {
                "First name",
                "Last name",
                "Count hour",
                "Date",
                "On Morning",
                "On Afternoon",
        };

        Object[][] datas = new Object[company.getEmployees().size() * company.getBiggestSizeWorkTodayList()][10];
        int index = 0;
        for (Employee employee : company.getEmployees()) {
            for (WorkToday workToday : employee.getWorkDaysCheck()) {
                datas[index][0] = employee.getFirstName();
                datas[index][1] = employee.getLastName();
                datas[index][2] = employee.getCountHour();
                datas[index][3] = workToday.getDate();
                if (workToday.getRealHourMorning() == null)
                    datas[index][4] = "null";
                else
                    datas[index][4] = workToday.getRealHourMorning() + " " + (employee.compareTimeMorning(workToday.getDate()) >= incidence ? employee.compareTimeMorning(workToday.getDate()) : "");
                if (workToday.getRealHourAfternoon() == null)
                    datas[index][5] = "null";
                else
                    datas[index][5] = workToday.getRealHourAfternoon() + " " + (employee.compareTimeAfternoon(workToday.getDate()) >= incidence ? employee.compareTimeAfternoon(workToday.getDate()) : "");
                index++;
            }
        }

        while (index < datas.length) {
            datas[index][0] = "";
            index++;
        }

        DefaultTableModel model = new DefaultTableModel(datas, columns) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        for (int i = model.getRowCount() - 1; i > 0 ; i--) {
            if (String.valueOf(model.getValueAt(i,0)).equals(""))
                model.removeRow(i);
        }
        tableChecks.setModel(model);

        tableChecks.setAutoCreateRowSorter(true);

        RowSorter.SortKey[] sks = new RowSorter.SortKey[]{
                new RowSorter.SortKey(3, SortOrder.DESCENDING)
        };
        tableChecks.getRowSorter().setSortKeys(Arrays.asList(sks));
        for(int i = 0; i < columns.length; i++) {
            if (i != 3)
                ((DefaultRowSorter)tableChecks.getRowSorter()).setSortable(i,false);
        }
    }


    /**
     * This function init the table with all the departments
     * @param departmentTable the table we are going to change the model
     */
    public static void initTableDepartment(JTable departmentTable) {
        String[] columns = {
                "ID",
                "Department name",
                "Number of employees",
                "Manager's name"
        };

        String[][] datas = new String[company.getDepartments().size()][4];
        int index = 0;
        for (Department department : company.getDepartments()) {
            datas[index] = department.toString().split(",");
            index++;
        }

        departmentTable.setModel(new DefaultTableModel(datas, columns) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        });

        departmentTable.setAutoCreateRowSorter(true);
        for(int i = 0; i < columns.length; i++) {
            ((DefaultRowSorter)departmentTable.getRowSorter()).setSortable(i,false);
        }
    }


    /**
     * This function init the table with all the employees without a department
     * @param employeesTable the table with all the employees without department
     */
    public static void initTableEmployeesWithoutDepartment(JTable employeesTable) {
        String[] columns = {
                "First name",
                "Last name",
                "Email",
                "ID",
                "Time Late",
                "Department ID"
        };

        List<Employee> employees = new ArrayList<>();

        for (Employee employee : company.getEmployees()) {
            if (employee.getIdDepartment() == -1) {
                employees.add(employee);
            }
        }

        int index = 0;
        String[][] datas = new String[employees.size()][6];
        for (Employee employee : employees) {
            datas[index] = employee.toString().split(",");
            index++;
        }

        employeesTable.setModel(new DefaultTableModel(datas, columns) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        });

        employeesTable.setAutoCreateRowSorter(true);

        for(int i = 0; i < columns.length; i++) {
            ((DefaultRowSorter)employeesTable.getRowSorter()).setSortable(i,false);
        }
    }


    /**
     * This function set the model of the table with all the departments without managers
     * @param departmentTable the table we are going to set the model
     */
    public static void initDepartmentTableWithoutManager(JTable departmentTable) {
        String[] columns = {
                "ID",
                "Department name",
                "Number of employees",
                "Manager's name"
        };

        List<Department> departments = new ArrayList<>();
        for (Department department : company.getDepartments()) {
            if (department.getManager() == null) {
                departments.add(department);
            }
        }

        Object[][] datas = new Object[departments.size()][4];
        int index = 0;
        for (Department department : departments) {
            datas[index] = department.toString().split(",");
            index++;
        }

        departmentTable.setModel(new DefaultTableModel(datas, columns) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        });

        departmentTable.setAutoCreateRowSorter(true);

        for(int i = 0; i < columns.length; i++) {
            ((DefaultRowSorter)departmentTable.getRowSorter()).setSortable(i,false);
        }
    }


    /**
     * This function define the action when the user click on a employee's first name in the tables where there are employees
     * @param employeeView the frame with the informations about the employee
     * @param employeesTable the table where there's the action
     * @return True if the frame opened else false
     */
    public boolean mouseActionEmployees(EmployeeView employeeView, JTable employeesTable) {
        int column = employeesTable.getSelectedColumn();
        if (column == 0) {
            int row = employeesTable.getSelectedRow();
            Object firstName = employeesTable.getValueAt(row, column);
            Object lastName = employeesTable.getValueAt(row, column + 1);
            seeEmployee(firstName, lastName, employeeView);
            return true;
        }
        if (column == 1) {
            int row = employeesTable.getSelectedRow();
            Object lastName = employeesTable.getValueAt(row, column);
            Object firstName = employeesTable.getValueAt(row, column - 1);
            seeEmployee(firstName, lastName, employeeView);
            return true;
        }
        return false;
    }


    /**
     * This function init the view of an employee
     * @param firstName the first name of the employee
     * @param lastName the last name of the employee
     * @param employeeView the view of the employee
     */
    public void seeEmployee(Object firstName, Object lastName, EmployeeView employeeView) {

        // Getting the employee and setting the contents of the view
        Employee employee = company.searchEmployee(String.valueOf(firstName), String.valueOf(lastName)); // Searching the employee
        employeeView.getFirstName().setText(employee.getFirstName()); // Setting the first name on the view
        employeeView.getLastName().setText(employee.getLastName()); // Setting the last name on the view
        employeeView.getMail().setText(employee.getEmail()); // Setting the email on the view
        employeeView.getCountHour().setText(String.valueOf(employee.getCountHour())); // Setting the late time

        // Searching for his department and setting the name on the view
        Department department = company.searchDepartment(employee.getIdDepartment());
        if (department == null)
            employeeView.getDepartmentName().setText("No department");
        else
            employeeView.getDepartmentName().setText(department.getDepartmentName());

        // Setting the table of hours
        String[] columns = {"Day", "Morning", "Afternoon"};
        Object[][] datas = new Object[5][5];
        int index = 0;
        for (DayOfWeek dayOfWeek : DayOfWeek.values()) {
            if (dayOfWeek == DayOfWeek.SUNDAY || dayOfWeek == DayOfWeek.SATURDAY)
                continue;
            datas[index][0] = dayOfWeek;
            datas[index][1] = employee.getCheckHoursDay(dayOfWeek).getInHour();
            datas[index][2] = employee.getCheckHoursDay(dayOfWeek).getOutHour();
            index++;
        }

        // Setting the model of the table
        employeeView.setHoursTable(new DefaultTableModel(datas, columns) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        });

        // Setting the table of all the checks
        String[] columns2 = {"Day", "On Morning", "On Afternoon"};
        Object[][] datas2 = new Object[employee.getWorkDaysCheck().size()][5];
        index = 0;
        for (WorkToday today : employee.getWorkDaysCheck()) {
            datas2[index][0] = today.getDate();
            if (today.getRealHourMorning() == null)
                datas2[index][1] = "null";
            else
                datas2[index][1] = today.getRealHourMorning() + " " + (employee.compareTimeMorning(today.getDate()) >= incidence ? employee.compareTimeMorning(today.getDate()) : "");
            if (today.getRealHourAfternoon() == null)
                datas2[index][2] = "null";
            else
                datas2[index][2] = today.getRealHourAfternoon() + " " + (employee.compareTimeAfternoon(today.getDate()) >= incidence ? employee.compareTimeAfternoon(today.getDate()) : "");
            index++;
        }

        // Setting the model of the table
        employeeView.setTableChecks(new DefaultTableModel(datas2, columns2) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        });

        employeeView.getTableChecks().setAutoCreateRowSorter(true);

        RowSorter.SortKey[] sks = new RowSorter.SortKey[]{
                new RowSorter.SortKey(0, SortOrder.DESCENDING)
        };
        employeeView.getTableChecks().getRowSorter().setSortKeys(Arrays.asList(sks));
        for(int i = 0; i < columns.length; i++) {
            ((DefaultRowSorter)employeeView.getTableChecks().getRowSorter()).setSortable(i,false);
        }

        // Creating the frame
        openFrame(employeeView);
    }


    /**
     * This function define the action on clicking on the departments' table
     * @param departmentView the frame with the informations about the department
     * @param departmentTable the table with the action
     * @return True if the frame opened else false
     */
    public boolean actionMouseDepartment(DepartmentView departmentView, JTable departmentTable) {
        int column = departmentTable.getSelectedColumn();
        if (column == 1) {
            int row = departmentTable.getSelectedRow();
            Object departmentName = departmentTable.getValueAt(row, column);
            seeDepartment(departmentName, departmentView);
            return true;
        }
        return false;
    }


    public boolean actionMouseDepartment(EmployeeView employeeView, JTable departmentTable) {
        int column = departmentTable.getSelectedColumn();
        if (column == 3) {
            int row = departmentTable.getSelectedRow();
            Object names = departmentTable.getValueAt(row, column);
            String firstName = String.valueOf(names).split(" ")[0];
            String lastName = String .valueOf(names).split(" ")[1];
            seeEmployee(firstName, lastName, employeeView);
            return true;
        }
        return false;
    }


    /**
     * This function init the view of a department
     * @param departmentName the department's name
     * @param departmentView the department's view
     */
    public void seeDepartment(Object departmentName, DepartmentView departmentView) {

        // Searching for the department
        Department department = company.searchDepartment(String.valueOf(departmentName));
        departmentView.getDepartmentName().setText(department.getDepartmentName());

        // Setting the contents of the manager
        if (department.getManager() != null)
            departmentView.getManagerLabel().setText(department.getManager().getFirstName()+" "+department.getManager().getLastName());
        else
            departmentView.getManagerLabel().setText("Doesn't have manager");

        // Creating the table of the employees
        String[] columns = {"First Name", "Last Name", "Email", "Time Late"};
        Object[][] datas = new Object[department.getNbEmployees()][4];
        int index = 0;
        for (Employee employee : department.getEmployees()) {
            datas[index][0] = employee.getFirstName();
            datas[index][1] = employee.getLastName();
            datas[index][2] = employee.getEmail();
            datas[index][3] = employee.getCountHour();
            index++;
        }

        // Setting if the cells are editable
        departmentView.setEmployeesTable(new DefaultTableModel(datas, columns) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        });

        departmentView.getEmployeesTable().setAutoCreateRowSorter(true);

        for(int i = 0; i < columns.length; i++) {
            ((DefaultRowSorter)departmentView.getEmployeesTable().getRowSorter()).setSortable(i,false);
        }

        // Creating the frame
        openFrame(departmentView);
    }
}