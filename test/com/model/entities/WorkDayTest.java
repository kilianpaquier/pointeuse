package com.model.entities;

import com.model.time.WorkToday;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalTime;

import static org.junit.Assert.*;

public class WorkDayTest {
    private WorkToday workToday;
    private WorkToday workToday2;
    private WorkToday workToday3;

    @Before
    public void setUp() {
        LocalTime realHourMorning = LocalTime.of(15,55);
        LocalTime realHourAfternoon = LocalTime.of(15,18);
        workToday = new WorkToday(realHourMorning, realHourAfternoon);

        workToday2 = new WorkToday(realHourMorning);
        workToday2.setRealHourAfternoon(realHourAfternoon);
        workToday2.setRealHourMorning(realHourMorning);

        workToday3 = new WorkToday();
        workToday3.setRealHourMorning(realHourMorning);
    }

    @Test
    public void testRoundTime(){
        try{
            assertEquals(LocalTime.of(16,0), workToday.getRealHourMorning());
            assertEquals(LocalTime.of(15,15), workToday.getRealHourAfternoon());
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void testSetHours(){
        try {
            assertEquals(LocalTime.of(16,0), workToday3.getRealHourMorning());
            assertEquals(LocalTime.of(16,0), workToday2.getRealHourMorning());
            assertEquals(LocalTime.of(15,15), workToday2.getRealHourAfternoon());

        } catch (Exception e){
            e.printStackTrace();
        }
    }
}