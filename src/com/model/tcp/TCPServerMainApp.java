package com.model.tcp;

import com.controller.CentralAppController;
import com.model.entities.Employee;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.List;

public class TCPServerMainApp extends TCPMessage implements Runnable {
    private String information = "noinfo";
    private List<Employee> employees = new ArrayList<>();
    private String ip;
    private int port;
    private boolean running = true;

    public boolean isRunning() {
        return running;
    }

    public TCPServerMainApp(String ip, int port) {
        s = null;
        isA = null;
        this.ip = ip;
        this.port = port;
    }

    public String getInformation() {
        return information;
    }

    /**
     * Setting the socket
     * @throws IOException if the port or the inetAddress is already used
     */
    private void setSocket() throws IOException {
        isA = new InetSocketAddress(ip, port);
        ss = new ServerSocket(isA.getPort());
        setStreamBuffer(ss.getReceiveBufferSize());
    }

    /**
     * This is the main function of the server, running on all time
     */
    @Override
    public void run() {
        InputStream in;
        try {
            setSocket();
            System.out.println("Socket set");
            // While the server is running, keep getting a communication
            while(running) {
                s = ss.accept(); // Waiting for a client
                in = s.getInputStream();
                information = readMessage(in); // Getting the message

                // Returning the list of employees if the message is "Get Employees"
                if (information.equals("Get Employees"))
                    sync();
                // Else, it seems that's a check
                else {
                    CentralAppController.getCheck(information);
                }
                in.close();
                s.close();
            }
        } catch (IOException e) {
            System.out.println("Socket closed");
        }
        finally {
            try {
                if (s != null)
                    s.close();
                ss.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * This function send all the employees as a string
     */
    private void sync() {
        OutputStream out;
        try {
            out = s.getOutputStream();
            String message = null;

            // Creating the message
            for (Employee employee : employees) {
                if (message == null)
                    message = employee.getIdEmployee() + "," + employee.getFirstName() + "," + employee.getLastName() + ";";
                else
                    message = message + employee.getIdEmployee() + "," + employee.getFirstName() + "," + employee.getLastName() + ";";
            }

            // Sending the employees
            writeMessage(out, message);
            System.out.println("Employees sent");
            s.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This function stop the server, when the main application is going to be off
     */
    public void close() {
        running = false;
        try {
            ss.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public List<Employee> getEmployees() {
        return employees;
    }
}
