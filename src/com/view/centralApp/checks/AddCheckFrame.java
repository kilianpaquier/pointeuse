package com.view.centralApp.checks;

import javax.swing.*;

public class AddCheckFrame extends JFrame {
    private JComboBox<Object> employeeBox;
    private JComboBox<Object> dateBox;
    private JComboBox<Object> hourBox;
    private JComboBox<Object> minuteBox;
    private JButton addCheckButton;
    private JPanel globalPanel;
    private JComboBox<Object> timeBox;

    public AddCheckFrame(String name) {
        super(name);
        setContentPane(globalPanel);
    }

    public JButton getAddCheckButton() {
        return addCheckButton;
    }

    public JComboBox<Object> getMinuteBox() {
        return minuteBox;
    }

    public JComboBox<Object> getHourBox() {
        return hourBox;
    }

    public JComboBox<Object> getDateBox() {
        return dateBox;
    }

    public JComboBox<Object> getEmployeeBox() {
        return employeeBox;
    }

    public JPanel getGlobalPanel() {
        return globalPanel;
    }

    public JComboBox<Object> getTimeBox() {
        return timeBox;
    }
}
