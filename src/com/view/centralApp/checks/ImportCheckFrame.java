package com.view.centralApp.checks;

import javax.swing.*;

public class ImportCheckFrame extends JFrame {
    private JTextField filePath;
    private JButton importChecksButton;
    private JPanel globalPane;

    public ImportCheckFrame(String name) {
        super(name);
        setContentPane(globalPane);
    }

    public JTextField getFilePath() {
        return filePath;
    }

    public JButton getImportChecksButton() {
        return importChecksButton;
    }
}
