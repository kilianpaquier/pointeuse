package com.model.entities;

import java.util.ArrayList;
import java.util.List;

/**
 * This class correspond to one department
 */
public class Department {
    // Attributes
    private static int nbDepartmentID = 0;
    private List<Employee> employees = new ArrayList<>();
    private Manager manager;
    private String departmentName;
    private int idDepartment;

    // Constructors

    /**
     * Here is the constructor with the name, one employee, the manager and an id
     * @param departmentName the name of department
     * @param firstEmployee one employee
     * @param manager the manager
     * @param idDepartment the id of department
     */
    public Department(String departmentName, Employee firstEmployee, Manager manager, int idDepartment){
        this.departmentName = departmentName;
        employees.add(firstEmployee);
        this.manager = manager;
        nbDepartmentID++;
        this.idDepartment = idDepartment;
    }

    /**
     * Here is the constructor with the name and an id
     * @param departmentName the name of department
     * @param idDepartment the id of department
     */
    public Department(String departmentName, int idDepartment){
        nbDepartmentID++;
        this.departmentName = departmentName;
        this.idDepartment = idDepartment;
    }

    /**
     * The constructor we will used in the application
     * @param text the name of the department
     */
    public Department(String text) {
        departmentName = text;
        nbDepartmentID++;
        this.idDepartment = nbDepartmentID;
    }

    // Methods
    public int getIdDepartment() {
        return idDepartment;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public Manager getManager() {
        return manager;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    /**
     * This function permits to add an employee (an object) to the department's list of employees
     * @param employee the object employee to add to the department
     * @exception IllegalArgumentException If the employee is already in a department or already in this department
     */
    public void addEmployees(Employee employee) {
        if (employees.contains(employee))
            throw new IllegalArgumentException("The employee is already in the list");
        employees.add(employee);
        employee.setIdDepartment(idDepartment);
    }

    /**
     * This function permits to set the manager of the department
     * @param manager the manager of the department
     */
    public void setManager(Manager manager) {
        manager.setIdDepartment(idDepartment);
        this.manager = manager;
    }

    /**
     * This function permit to delete an employee from the department by searching with is frist name and last name
     * We are not destrying his, we're deleting him from the list and setting his despartment id to -1
     * @param firstName the first name of the employee to delete to the department
     * @param lastName the last nam eof the employee to delete to the department
     */
    public void deleteEmployee(String firstName, String lastName){
        for (Employee employee : employees){
            if (employee.getLastName().equalsIgnoreCase(lastName) && employee.getFirstName().equalsIgnoreCase(firstName)){
                employee.setIdDepartment(-1);
                employees.remove(employee);
                break;
            }
        }
        if (manager != null) {
            if (manager.getFirstName().equalsIgnoreCase(firstName) && manager.getLastName().equalsIgnoreCase(lastName)) {
                manager.setIdDepartment(-1);
                manager = null;
            }
        }
    }

    public int getNbEmployees() {
       return employees.size();
    }

    /**
     * This function is used when the department is delete from the company
     * It permits to set all idDepartment of employees and manager to -1 and also to delete employees from the list
     */
    public void deleteAll(){
        for (Employee employee : employees){
            employee.setIdDepartment(-1);
        }
        employees.clear();
        employees = null;
        if (manager != null) {
            manager.setIdDepartment(-1);
            manager = null;
        }
    }

    /**
     * This function returns an employee if he's found in the list of the department
     * @param firstName the first name of the employee to search
     * @param lastName the last name of the employee to search
     * @return An employee if he's found or null
     */
    public Employee searchEmployee(String firstName, String lastName) {
        for (Employee employee : employees){
            if (firstName.equals(employee.getFirstName()) && lastName.equals(employee.getLastName())) {
                return employee;
            }
        }
        return null;
    }

    /**
     * This function returns the biggest size of the workToday's list
     * @return returns the size
     */
    public int getBiggestSizeWorkTodaysList() {
        int size = 0;
        for (Employee employee : employees){
            if (employee.getWorkDaysCheck().size() > size)
                size = employee.getWorkDaysCheck().size();
        }
        return size;
    }

    @Override
    public String toString() {
        if (manager != null)
            return getIdDepartment()+","+getDepartmentName()+","+employees.size()+","+manager.getFirstName()+" "+manager.getLastName();
        else
            return getIdDepartment()+","+getDepartmentName()+","+employees.size()+","+"null";
    }
}
