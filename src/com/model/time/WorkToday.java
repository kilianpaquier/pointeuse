package com.model.time;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * This class correspond to when the employee checked in a day
 */
public class WorkToday {
    // Attributes
    private LocalTime realHourMorning;
    private LocalTime realHourAfternoon;
    private LocalDate date;

    // Constructors
    public WorkToday(){

    }

    /**
     * The constructor creating a check time on morning
     * @param realHourMorning the time the employee checked this morning
     */
    public WorkToday(LocalTime realHourMorning){
        this.realHourMorning = LocalTime.of(realHourMorning.getHour(),realHourMorning.getMinute(),0,0);
        roundTimeMorning();
    }

    /**
     * The constructor creating a check time with the hour of checking this morning and this afternoon
     * @param realHourMorning the time the employee checked this morning
     * @param realHourAfternoon the time the employee checked this afternoon
     */
    public WorkToday(LocalTime realHourMorning, LocalTime realHourAfternoon) {
        this.realHourMorning = LocalTime.of(realHourMorning.getHour(),realHourMorning.getMinute(),0,0);
        this.realHourAfternoon = LocalTime.of(realHourAfternoon.getHour(), realHourAfternoon.getMinute(),0,0);
        roundTimeMorning();
        roundTimeAfternoon();
    }

    // Methods

    /**
     * Rounding the time the employee checked this morning
     */
    public void roundTimeMorning() {
        int minute = realHourMorning.getMinute();
        int hour = realHourMorning.getHour();
        int[] timeResult = round(minute,hour);
        if (timeResult[1] == 24)
            realHourMorning = realHourMorning.withHour(0);
        else
            realHourMorning = realHourMorning.withHour(timeResult[1]);
        realHourMorning = realHourMorning.withMinute(timeResult[0]);
        realHourMorning = realHourMorning.withSecond(0);
        realHourMorning = realHourMorning.withNano(0);
    }

    /**
     * Rounding the time the employee checked this afternoon
     */
    public void roundTimeAfternoon() {
        int minute = realHourAfternoon.getMinute();
        int hour = realHourAfternoon.getHour();
        int[] timeResult = round(minute,hour);
        if (timeResult[1] == 24)
            realHourAfternoon = realHourAfternoon.withHour(0);
        else
            realHourAfternoon = realHourAfternoon.withHour(timeResult[1]);
        realHourAfternoon = realHourAfternoon.withMinute(timeResult[0]);
        realHourAfternoon = realHourAfternoon.withSecond(0);
        realHourAfternoon = realHourAfternoon.withNano(0);
    }

    public LocalTime getRealHourMorning() {
        return realHourMorning;
    }

    public LocalTime getRealHourAfternoon() {
        return realHourAfternoon;
    }

    public void setRealHourMorning(LocalTime realHourMorning) {
        this.realHourMorning = LocalTime.of(realHourMorning.getHour(),realHourMorning.getMinute(),0,0);
        roundTimeMorning();
    }

    public void setRealHourAfternoon(LocalTime realHourAfternoon) {
        this.realHourAfternoon = LocalTime.of(realHourAfternoon.getHour(),realHourAfternoon.getMinute(),0,0);
        roundTimeAfternoon();
    }

    /**
     * A function round which take in arguments the minute and the hour
     * @param minute the minute to round
     * @param hour the hour to increment if the minute is rounded from 53 to 0
     * @return An array with the minute and the hour rounded
     */
    private int[] round(int minute, int hour){
        if (minute <= 7)
            minute = 0;
        else if (minute <= 22)
            minute = 15;
        else if (minute <= 37)
            minute = 30;
        else if (minute <= 52)
            minute = 45;
        else {
            minute = 0;
            hour++;
        }
        return new int[]{minute, hour};
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalDate getDate() {
        return date;
    }
}
