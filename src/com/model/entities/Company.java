package com.model.entities;

import com.model.time.WorkToday;

import javax.swing.*;
import java.io.*;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

/**
 * This class correspond to the company
 */
public class Company {

    // Attributes
    private List<Department> departments = new ArrayList<>();
    private List<Employee> employees = new ArrayList<>();
    private Boss boss;

    // Constructors

    /**
     * Default constructor
     */
    public Company() {
    }

    /**
     *
     * @param theBoss the boss the copy in our company's boss
     */
    public Company(Boss theBoss){
        boss = theBoss;
    }

    /**
     * Creating a company with a department, a boss and a employee
     * @param firstDepartment the first department
     * @param firstEmployee the first employee
     * @param theBoss the boss
     */
    public Company(Department firstDepartment, Employee firstEmployee, Boss theBoss){
        departments.add(firstDepartment);
        employees.add(firstEmployee);
        boss = theBoss;
    }

    // Methods
    public List<Department> getDepartments() {
        return departments;
    }

    public void setBoss(Boss boss) {
        this.boss = boss;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public Boss getBoss() {
        return boss;
    }

    /**
     * Here is the function to add a department to the company
     * @param department the department to add to the company
     * @exception IllegalArgumentException If the department is already in list
     */
    public void addDepartments(Department department) {
        String name = department.getDepartmentName();
        if(searchDepartment(name) != null)
            throw new IllegalArgumentException("Department already in list");
        else
            departments.add(department);
    }

    /**
     * Here is the function to add an employee to the company
     * @param employee the employee to add
     * @exception IllegalArgumentException If the employee is already in list
     */
    public void addEmployee(Employee employee){
        if (searchEmployee(employee.getFirstName(), employee.getLastName()) != null)
            throw new IllegalArgumentException("Employee already in list");
        else
            employees.add(employee);
    }

    public int getNumberDepartments(){
        return departments.size();
    }

    public int getNumberEmployees(){
        return employees.size();
    }

    /**
     * Here is the function to delete an employee from the company
     * @param firstName first name of the employee to delete
     * @param lastName last name of the employee to delete
     */
    public void deleteEmployee(String firstName, String lastName){
        for (Employee employee : employees){
            if (firstName.equalsIgnoreCase(employee.getFirstName()) && lastName.equalsIgnoreCase(employee.getLastName())){
                int idDepartment = employee.getIdDepartment();
                Department department = searchDepartment(idDepartment);
                if (department != null)
                    department.deleteEmployee(firstName, lastName);
                employees.remove(employee);
                break;
            }
        }
    }

    /**
     * Here is the function to delete a department from the company
     * @param departmentName the department name to delete
     */
    public void deleteDepartment(String departmentName){
        for (Department department : departments){
            if (department.getDepartmentName().equalsIgnoreCase(departmentName)){
                department.deleteAll(); // Setting all the idDepartment of the employees to -1, same for the manager
                departments.remove(department);
                break;
            }
        }
    }

    /**
     * The function of saving the data in a file
     * This function is run every time the central app is closed
     * @throws IOException Thrown if there's an error in writing the file
     */
    public void saveDatas() throws IOException {
            FileOutputStream file = new FileOutputStream(".\\data\\departments.csv"); // Creating the file where we are going to write our information
            PrintWriter object = new PrintWriter(file); // Creating the printer

            // Saving the departments
            for (Department department : departments){
                object.write(String.valueOf(department.getIdDepartment()).trim()); // Saving the id
                object.write(',');
                object.write(department.getDepartmentName()); // Saving the name
                object.write("\n");
            }
            object.close();
            file.close();

            exportCSV(".\\data\\employees.csv"); // Saving the employees in another file
    }

    /**
     * Here is the function to find a department in the company by an id
     * @param idDepartment the id of the department to search
     * @return the department if it's found
     */
    public Department searchDepartment(int idDepartment) {
        for(Department department : departments){
            if (department.getIdDepartment() == idDepartment){
                return department;
            }
        }
        return null;
    }

    /**
     * Here is the function to find a department in the company by a name
     * @param departmentName the name of the department to search
     * @return the department if it's found
     */
    public Department searchDepartment(String departmentName) {
        for(Department department : departments){
            if (department.getDepartmentName().equals(departmentName)){
                return department;
            }
        }
        return null;
    }

    /**
     * This function returns the biggest size of all list of workTodays
     * @return the biggest size
     */
    public int getBiggestSizeWorkTodayList() {
        int size = 0;
        for (Employee employee : employees) {
            if (employee.getWorkDaysCheck().size() > size)
                size = employee.getWorkDaysCheck().size();
        }
        return size;
    }

    /**
     * This function is used to change an employee from a department into another department
     * @param department the new department of the employee
     * @param employee the employee who need to change of department
     */
    public void changeDepartmentOfEmployee(Department department, Employee employee) {
        if (department.getManager() != null) {
            department.addEmployees(employee);
            for (Department department1 : departments) {
                if (department1.getEmployees().contains(employee) && employee.getIdDepartment() != department1.getIdDepartment())
                    department1.getEmployees().remove(employee);
            }
        }
        else {
            int answer = JOptionPane.showConfirmDialog(null,
                    "This department doesn't have a managing manager, do you want to define this employee as manager of this department ?",
                    "Confirmation",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE);
            if (answer == JOptionPane.YES_OPTION) {
                for (Department department1 : departments) {
                    if (department1.getEmployees().contains(employee) && employee.getIdDepartment() != department1.getIdDepartment())
                        department1.getEmployees().remove(employee);
                }
                Manager manager = new Manager(employee);
                deleteEmployee(employee.getFirstName(), employee.getLastName());
                addEmployee(manager);
                department.setManager(manager);
            }
            else {
                department.addEmployees(employee);
                for (Department department1 : departments) {
                    if (department1.getEmployees().contains(employee) && employee.getIdDepartment() != department1.getIdDepartment())
                        department1.getEmployees().remove(employee);
                }
            }
        }
    }

    /**
     * This function search and return all the manager and employees which are not managing a department
     * @return The list of all managers and employees which are not managing any department
     */
    public List<Employee> getManagersNotManagingWithEmployees() {
        List<Employee> managers = new ArrayList<>();
        for (Employee employee : employees){
                if (!isManaging(employee))
                    managers.add(employee);
        }
        return managers;
    }

    /**
     * This function search and return all the manager which are not managing a department
     * @return The list of all managers which are in employees' list of each department
     */
    public List<Employee> getManagersNotManaging() {
        List<Employee> managers = new ArrayList<>();
        for (Employee employee : employees){
            if (employee.getClass().getSimpleName().equals("Manager")) {
                if (!isManaging(employee))
                    managers.add(employee);
            }
        }
        return managers;
    }

    /**
     * This function compare all the managers of departments with the employee pasted in argument (manager)
     * @param employee the employee (manager) who we need to know if a manage a department
     * @return false if he doesn't manage, true else
     */
    public boolean isManaging(Employee employee){
        if (employee.getClass().getSimpleName().equals("Employee"))
            return false;
        for (Department department : departments){
            if (department.getManager() == null)
                continue;
            if (department.getManager().equals(employee))
                return true;
        }
        return false;
    }

    /**
     * This function has to purpose to return all the managers managing the company
     * @return the list of all the managers managing the departments
     */
    public List<Manager> getManagersManaging() {
        List<Manager> managers = new ArrayList<>();
        for (Department department : departments) {
            managers.add(department.getManager());
        }
        return managers;
    }

    /**
     * Here is the function to find an employee in the company by an id
     * @param firstName the first name to find
     * @param lastName the last name to find
     * @return The employee if he's found
     */
    public Employee searchEmployee(String firstName, String lastName){
        for (Employee employee : employees){
            if(employee.getLastName().equals(lastName) && employee.getFirstName().equals(firstName)){
                return employee;
            }
        }
        return null;
    }

    /**
     * This function permit to import from a CSV a list of employees
     * @param fileName the filename (need to have all the way to the filename)
     * @throws IOException Thrown if the file doesn't exist
     */
    public void importCSV(String fileName) throws IOException {
        // Creating the buffer to read the file CSV
        BufferedReader object = new BufferedReader(new FileReader(fileName));

        // Creating the line
        String line;

        // reading the line while it's not null
        while ((line = object.readLine()) != null){
            String[] result = line.split(","); // Splitting the line into a result with all information

            // The global if, permit to know if we're reading an employee or a manager
            if(result[0].equals("Employee")) {
                Employee employee = new Employee(/*Integer.valueOf(result[1].trim()),*/ result[2], result[3], result[4]); // Creating the employee
                employee.setIdDepartment(Integer.valueOf(result[5].trim())); // Setting his department
                employee.addCountHour(Integer.valueOf(result[6].trim())); // Setting his late time

                // Setting the hours the employee to be here during the week
                int indexForInHour = 7;
                int indexForOutHour = 8;
                for(DayOfWeek dayOfWeek : DayOfWeek.values()){
                    if (dayOfWeek == DayOfWeek.SATURDAY || dayOfWeek == DayOfWeek.SUNDAY)
                        continue;
                    String[] splitInHour = result[indexForInHour].split(":"); // Splitting the hour and the minutes of the beginning time
                    String[] splitOutHour = result[indexForOutHour].split(":"); // Splitting the hour and the minutes of the ending time
                    employee.setTheoryCheckHours(dayOfWeek,LocalTime.of(Integer.valueOf(splitInHour[0].trim()),Integer.valueOf(splitInHour[1].trim())), LocalTime.of(Integer.valueOf(splitOutHour[0].trim()), Integer.valueOf(splitOutHour[1]))); // Creating the hours of begin and end of the day
                    indexForInHour += 2;
                    indexForOutHour += 2;
                }

                // Setting the list of days the employee already did (check in/out)
                int indexForMorning = 17;
                int indexForAfternoon = 18;
                int indexForDate = 19;
                for (int index = 17; index < result.length; index += 3){
                    String[] splitDate = result[indexForDate].split("-");
                    LocalDate date = LocalDate.of(Integer.valueOf(splitDate[0]), Integer.valueOf(splitDate[1]), Integer.valueOf(splitDate[2]));

                    if(!result[indexForMorning].equals("null")) {
                        String[] splitMorning = result[indexForMorning].split(":"); // Splitting the hour and the minutes
                        employee.setWorkDaysCheck(LocalTime.of(Integer.valueOf(splitMorning[0].trim()), Integer.valueOf(splitMorning[1].trim())), date); // Creating his checking time
                    }
                    else
                        employee.setWorkDaysCheck(null, date);

                    if(!result[indexForAfternoon].equals("null")) {
                        String[] splitAfternoon = result[indexForAfternoon].split(":"); // Splitting the hour and the minutes
                        employee.addAfternoonTimeWorkToday(LocalTime.of(Integer.valueOf(splitAfternoon[0].trim()), Integer.valueOf(splitAfternoon[1].trim())));
                    }

                    indexForMorning += 3;
                    indexForAfternoon += 3;
                    indexForDate += 3;
                }

                addEmployee(employee); // Adding the employee to the company
                Department department = searchDepartment(employee.getIdDepartment()); // Searching if the employee is in a department
                if(department != null)
                    department.addEmployees(employee); // If he is, adding him to
                else
                    employee.setIdDepartment(-1); // If not we set his id to -1
            }
            // Doing the same if we are creating a manager
            else {
                Manager manager = new Manager(/*Integer.valueOf(result[1].trim()),*/ result[2], result[3], result[4]); // Creating the manager
                manager.setIdDepartment(Integer.valueOf(result[5].trim())); // Setting his department id
                manager.addCountHour(Integer.valueOf(result[6].trim())); // Setting his late time

                // Setting the hours the employee to be here during the week
                int indexForInHour = 7;
                int indexForOutHour = 8;
                for(DayOfWeek dayOfWeek : DayOfWeek.values()){
                    if (dayOfWeek == DayOfWeek.SATURDAY || dayOfWeek == DayOfWeek.SUNDAY)
                        continue;
                    String[] splitInHour = result[indexForInHour].split(":"); // Splitting the hour and the minutes
                    String[] splitOutHour = result[indexForOutHour].split(":"); // Splitting the hour and the minutes
                    manager.setTheoryCheckHours(dayOfWeek,LocalTime.of(Integer.valueOf(splitInHour[0].trim()),Integer.valueOf(splitInHour[1].trim())), LocalTime.of(Integer.valueOf(splitOutHour[0].trim()), Integer.valueOf(splitOutHour[1])));
                    indexForInHour += 2;
                    indexForOutHour += 2;
                }

                // Setting the list of days the employee already did (check in/out)
                int indexForMorning = 17;
                int indexForAfternoon = 18;
                int indexForDate = 19;
                for (int index = 17; index < result.length; index += 3){
                    String[] splitDate = result[indexForDate].split("-");
                    LocalDate date = LocalDate.of(Integer.valueOf(splitDate[0]), Integer.valueOf(splitDate[1]), Integer.valueOf(splitDate[2]));

                    if(!result[indexForMorning].equals("null")) {
                        String[] splitMorning = result[indexForMorning].split(":"); // Splitting the hour and the minutes
                        manager.setWorkDaysCheck(LocalTime.of(Integer.valueOf(splitMorning[0].trim()), Integer.valueOf(splitMorning[1].trim())), date); // Creating his checking time
                    }
                    else
                        manager.setWorkDaysCheck(null, date);

                    if(!result[indexForAfternoon].equals("null")) {
                        String[] splitAfternoon = result[indexForAfternoon].split(":"); // Splitting the hour and the minutes
                        manager.addAfternoonTimeWorkToday(LocalTime.of(Integer.valueOf(splitAfternoon[0].trim()), Integer.valueOf(splitAfternoon[1].trim())));
                    }

                    indexForMorning += 3;
                    indexForAfternoon += 3;
                    indexForDate += 3;
                }

                // Adding the employee to his department and to the company
                addEmployee(manager);
                Department department = searchDepartment(manager.getIdDepartment());
                if(department != null)
                    department.setManager(manager);
            }
        }
        object.close();
    }

    /**
     * This function permit to export to a csv file the list of the employees
     * @param fileName the filename (need to have all the way to the filename)
     * @throws IOException Thrown if there's an error in writing the document
     */
    public void exportCSV(String fileName) throws IOException {
        FileOutputStream file = new FileOutputStream(fileName); // Creating the file where we are going to write our information
        PrintWriter object = new PrintWriter(file); // Creating the printer

        // Saving the employees in many and many columns because there's many information
        for (Employee employee : employees) {
            object.write(employee.getClass().getSimpleName()); // Saving the name class (employee or manager)
            object.write(',');
            object.write(String.valueOf(employee.getIdEmployee()).trim()); // Saving the id
            object.write(',');
            object.write(employee.getFirstName()); // Saving his first name
            object.write(',');
            object.write(employee.getLastName()); // Saving his last name
            object.write(',');
            object.write(employee.getEmail() == null ? "null" : employee.getEmail()); // Saving his email, maybe null
            object.write(',');
            object.write(String.valueOf(employee.getIdDepartment()).trim()); // Saving his department id
            object.write(',');
            object.write(String.valueOf(employee.getCountHour()).trim()); // Saving his late time
            object.write(',');

            // Saving all his hours of beginning and ending of the week
            for(DayOfWeek day : DayOfWeek.values()){
                if (day == DayOfWeek.SATURDAY || day == DayOfWeek.SUNDAY)
                    continue;
                object.write(employee.getCheckHoursDay(day).getInHour().toString()); // Saving the beginning
                object.write(',');
                object.write(employee.getCheckHoursDay(day).getOutHour().toString()); // Saving the ending
                object.write(',');
            }

            // Saving the checks in he did
            List<WorkToday> workTodayList = employee.getWorkDaysCheck();
            for (int index = 0; index < workTodayList.size(); index++) {
                // Writing the morning
                if (workTodayList.get(index).getRealHourMorning() == null)
                    object.write("null");
                else
                    object.write(workTodayList.get(index).getRealHourMorning().toString()); // Writing the hour of check at the beginning of the day
                object.write(',');

                // Writing the afternoon
                if (workTodayList.get(index).getRealHourAfternoon() == null)
                    object.write("null");
                else
                    object.write(workTodayList.get(index).getRealHourAfternoon().toString()); // Writing the hour of check at the end of the day
                object.write(',');

                // Writing the date
                object.write(workTodayList.get(index).getDate().toString());
                if (index != workTodayList.size()-1)
                    object.write(',');
            }
            object.write("\n");
        }

        object.close();
        file.close();
    }

    /**
     * Run at the beginning of the Central App, it permits to create the company and the employees from the previous save
     * @throws IOException Thrown if the file is not found
     */
    public void readDatas() throws IOException {
        BufferedReader object = new BufferedReader(new FileReader(".\\data\\departments.csv")); // Creating the buffer

        // Creating all the lines

        // Creating one line
        String line;
        // While the line isn't null we continue
        while ((line = object.readLine()) != null){
            String[] result = line.split(","); // Splitting the line into columns to have all information
            Department department = new Department(result[1], Integer.valueOf(result[0].trim())); // Creating the department with the name and the id
            addDepartments(department); // Adding the department to the company
        }

        object.close();

        importCSV(".\\data\\employees.csv"); // Reading our employees file
    }


    /**
     * This is the stub function, to initialise the company with some employees, managers and departments
     * \Warning This function is only need to be used when the company is creating
     * @return The company
     */
    public Company stubMethod() {
        // Creating the departments (10)
        int nbEmployees = 0;
        int increment = 1;
        for (int nbDepartments = 0; nbDepartments < 10; nbDepartments++) {
            Department department = new Department("department"+nbDepartments); // Creating the department
            addDepartments(department); // Adding the department to the company

            // Creating 10 employees per department
            for (; nbEmployees < 10 * increment; nbEmployees++) {
                Employee employee = new Employee("employee"+nbEmployees,"employee"+nbEmployees, "employee"+nbEmployees+"@company.com");

                // Init his days of work
                for (DayOfWeek day : DayOfWeek.values()){
                    if (day == DayOfWeek.SATURDAY || day == DayOfWeek.SUNDAY)
                        continue;
                    employee.setTheoryCheckHours(day, LocalTime.of(8,0,0,0), LocalTime.of(18,15,0,0));
                }

                // Adding the employee to the company and to the department
                addEmployee(employee);
                department.addEmployees(employee);
            }
            increment++;

            // Creating the manager of the department
            Manager manager = new Manager("manager"+nbDepartments,"manager"+nbDepartments,"manager"+nbDepartments+"@company.com");

            // Init his days of work
            for (DayOfWeek day : DayOfWeek.values()){
                if (day == DayOfWeek.SATURDAY || day == DayOfWeek.SUNDAY)
                    continue;
                manager.setTheoryCheckHours(day, LocalTime.of(8,0,0,0), LocalTime.of(18,15,0,0));
            }

            // Adding the employee to the company and to the department
            addEmployee(manager);
            department.setManager(manager);
        }
        return this;
    }


    /**
     * This function import some checks from a file
     * @param filename the path of the file
     * @throws IOException an exception if the file is not found
     */
    public void importChecks(String filename) throws IOException {
        BufferedReader object = new BufferedReader(new FileReader(filename)); // Creating the buffer
        boolean found;

        // Creating one line
        String line;

        // While the line isn't null we continue
        while ((line = object.readLine()) != null){
            found = false;
            String[] result = line.split(","); // Splitting the line into columns to have all information
            Employee employee = searchEmployee(result[0], result[1]);
            if (employee != null) {
                LocalDate date = LocalDate.of(Integer.valueOf(result[2].split("-")[0]), Integer.valueOf(result[2].split("-")[1]), Integer.valueOf(result[2].split("-")[2]));
                if (date.isAfter(LocalDate.now()))
                    continue;
                else {
                    for (WorkToday workToday : employee.getWorkDaysCheck()) {
                        if (workToday.getDate().equals(date)) {
                            found = true;
                        }
                    }
                    if (found)
                        continue;
                    else {
                        if (result[3].equals("null") && !result[4].equals("null") && (date.getDayOfWeek() != DayOfWeek.SUNDAY && date.getDayOfWeek() != DayOfWeek.SATURDAY)) {
                            String[] time = result[4].split(":");
                            employee.setWorkDaysCheck(null, date);
                            employee.addAfternoonTimeWorkToday(LocalTime.of(Integer.valueOf(time[0]), Integer.valueOf(time[1]), Integer.valueOf(time[2])));
                            employee.addCountHour(employee.compareTimeAfternoon(date));
                        } else if (!result[3].equals("null") && result[4].equals("null") && (date.getDayOfWeek() != DayOfWeek.SUNDAY && date.getDayOfWeek() != DayOfWeek.SATURDAY)) {
                            String[] time = result[3].split(":");
                            employee.setWorkDaysCheck(LocalTime.of(Integer.valueOf(time[0]), Integer.valueOf(time[1]), Integer.valueOf(time[2])), date);
                            employee.addCountHour(employee.compareTimeMorning(date));
                        } else if (!result[3].equals("null") && !result[4].equals("null") && (date.getDayOfWeek() != DayOfWeek.SUNDAY && date.getDayOfWeek() != DayOfWeek.SATURDAY)) {
                            String[] morning = result[3].split(":");
                            String[] time = result[4].split(":");
                            employee.setWorkDaysCheck(LocalTime.of(Integer.valueOf(morning[0]), Integer.valueOf(morning[1]), Integer.valueOf(morning[2])), date);
                            employee.addAfternoonTimeWorkToday(LocalTime.of(Integer.valueOf(time[0]), Integer.valueOf(time[1]), Integer.valueOf(time[2])));
                            employee.addCountHour(employee.compareTimeMorning(date));
                            employee.addCountHour(employee.compareTimeAfternoon(date));
                        }
                    }
                }
            }
        }
        object.close();
    }
}
